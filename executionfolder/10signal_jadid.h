


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









typedef enum {
  if_G_constant,
  if_Y_constant,
  if_R_constant
} if_sigs_type;

extern const char* if_sigs_name[];

#define if_sigs_copy(x,y) (x)=(y)
#define if_sigs_compare(x,y) (x)-(y)
#define if_sigs_print(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_xmlize(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_reset(x) (x)=(if_sigs_type)0
#define if_sigs_iterate(x) for(x=(if_sigs_type)0;x<=if_R_constant;x=(if_sigs_type)(x + 1))
#define if_sigs_eq(x,y) if_sigs_compare(x,y)==0
#define if_sigs_ne(x,y) if_sigs_compare(x,y)!=0


/* 
 * twosignal [system] instance interface 
 *
 */

class if_twosignal_instance : public IfInstance { 

  public:
    if_twosignal_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_twosignal_instance(const if_twosignal_instance&);

  protected:

};


/* 
 * bridge instance interface 
 *
 */
 
#define if_bridge_process 1



typedef if_void_type if_bridge_par_type;

#define if_bridge_par_copy(x,y) if_void_copy(x,y)
#define if_bridge_par_compare(x,y) if_void_compare(x,y)
#define if_bridge_par_print(x,f) if_void_print(x,f)
#define if_bridge_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_bridge_par_reset(x) if_void_reset(x)
#define if_bridge_par_iterate(x) if_void_iterate(x)
#define if_bridge_par_eq(x,y) if_bridge_par_compare(x,y)==0
#define if_bridge_par_ne(x,y) if_bridge_par_compare(x,y)!=0


typedef struct {
  if_clock_type x1;
  if_clock_type y1;
  if_clock_type z1;
  if_clock_type x2;
  if_clock_type y2;
  if_clock_type z2;
  if_clock_type x3;
  if_clock_type y3;
  if_clock_type z3;
  if_clock_type x4;
  if_clock_type y4;
  if_clock_type z4;
  if_clock_type x5;
  if_clock_type y5;
  if_clock_type z5;
  if_clock_type x6;
  if_clock_type y6;
  if_clock_type z6;
  if_clock_type x7;
  if_clock_type y7;
  if_clock_type z7;
  if_clock_type x8;
  if_clock_type y8;
  if_clock_type z8;
  if_clock_type x9;
  if_clock_type y9;
  if_clock_type z9;
  if_clock_type x10;
  if_clock_type y10;
  if_clock_type z10;
  if_sigs_type sig1;
  if_sigs_type sig2;
  if_sigs_type sig3;
  if_sigs_type sig4;
  if_sigs_type sig5;
  if_sigs_type sig6;
  if_sigs_type sig7;
  if_sigs_type sig8;
  if_sigs_type sig9;
  if_sigs_type sig10;
  if_clock_type F;
  if_clock_type Q;
  if_boolean_type inls;
} if_bridge_var_type;

inline void if_bridge_var_copy
    (if_bridge_var_type& x, const if_bridge_var_type y);
inline int if_bridge_var_compare
    (const if_bridge_var_type x, const if_bridge_var_type y);
inline void if_bridge_var_print
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_xmlize
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_reset
    (if_bridge_var_type& x);
#define if_bridge_var_iterate(x)\
  if_clock_iterate(x.x1)\
  if_clock_iterate(x.y1)\
  if_clock_iterate(x.z1)\
  if_clock_iterate(x.x2)\
  if_clock_iterate(x.y2)\
  if_clock_iterate(x.z2)\
  if_clock_iterate(x.x3)\
  if_clock_iterate(x.y3)\
  if_clock_iterate(x.z3)\
  if_clock_iterate(x.x4)\
  if_clock_iterate(x.y4)\
  if_clock_iterate(x.z4)\
  if_clock_iterate(x.x5)\
  if_clock_iterate(x.y5)\
  if_clock_iterate(x.z5)\
  if_clock_iterate(x.x6)\
  if_clock_iterate(x.y6)\
  if_clock_iterate(x.z6)\
  if_clock_iterate(x.x7)\
  if_clock_iterate(x.y7)\
  if_clock_iterate(x.z7)\
  if_clock_iterate(x.x8)\
  if_clock_iterate(x.y8)\
  if_clock_iterate(x.z8)\
  if_clock_iterate(x.x9)\
  if_clock_iterate(x.y9)\
  if_clock_iterate(x.z9)\
  if_clock_iterate(x.x10)\
  if_clock_iterate(x.y10)\
  if_clock_iterate(x.z10)\
  if_sigs_iterate(x.sig1)\
  if_sigs_iterate(x.sig2)\
  if_sigs_iterate(x.sig3)\
  if_sigs_iterate(x.sig4)\
  if_sigs_iterate(x.sig5)\
  if_sigs_iterate(x.sig6)\
  if_sigs_iterate(x.sig7)\
  if_sigs_iterate(x.sig8)\
  if_sigs_iterate(x.sig9)\
  if_sigs_iterate(x.sig10)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_boolean_iterate(x.inls)
inline if_bridge_var_type if_bridge_var_make
    (if_clock_type x1,if_clock_type y1,if_clock_type z1,if_clock_type x2,if_clock_type y2,if_clock_type z2,if_clock_type x3,if_clock_type y3,if_clock_type z3,if_clock_type x4,if_clock_type y4,if_clock_type z4,if_clock_type x5,if_clock_type y5,if_clock_type z5,if_clock_type x6,if_clock_type y6,if_clock_type z6,if_clock_type x7,if_clock_type y7,if_clock_type z7,if_clock_type x8,if_clock_type y8,if_clock_type z8,if_clock_type x9,if_clock_type y9,if_clock_type z9,if_clock_type x10,if_clock_type y10,if_clock_type z10,if_sigs_type sig1,if_sigs_type sig2,if_sigs_type sig3,if_sigs_type sig4,if_sigs_type sig5,if_sigs_type sig6,if_sigs_type sig7,if_sigs_type sig8,if_sigs_type sig9,if_sigs_type sig10,if_clock_type F,if_clock_type Q,if_boolean_type inls);
#define if_bridge_var_eq(x,y) if_bridge_var_compare(x,y)==0
#define if_bridge_var_ne(x,y) if_bridge_var_compare(x,y)!=0

class if_bridge_instance : public if_twosignal_instance {

public:
  
  if_bridge_instance();
  if_bridge_instance(const if_bridge_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& x1()
    { return m_var.x1; }
  inline if_clock_type& y1()
    { return m_var.y1; }
  inline if_clock_type& z1()
    { return m_var.z1; }
  inline if_clock_type& x2()
    { return m_var.x2; }
  inline if_clock_type& y2()
    { return m_var.y2; }
  inline if_clock_type& z2()
    { return m_var.z2; }
  inline if_clock_type& x3()
    { return m_var.x3; }
  inline if_clock_type& y3()
    { return m_var.y3; }
  inline if_clock_type& z3()
    { return m_var.z3; }
  inline if_clock_type& x4()
    { return m_var.x4; }
  inline if_clock_type& y4()
    { return m_var.y4; }
  inline if_clock_type& z4()
    { return m_var.z4; }
  inline if_clock_type& x5()
    { return m_var.x5; }
  inline if_clock_type& y5()
    { return m_var.y5; }
  inline if_clock_type& z5()
    { return m_var.z5; }
  inline if_clock_type& x6()
    { return m_var.x6; }
  inline if_clock_type& y6()
    { return m_var.y6; }
  inline if_clock_type& z6()
    { return m_var.z6; }
  inline if_clock_type& x7()
    { return m_var.x7; }
  inline if_clock_type& y7()
    { return m_var.y7; }
  inline if_clock_type& z7()
    { return m_var.z7; }
  inline if_clock_type& x8()
    { return m_var.x8; }
  inline if_clock_type& y8()
    { return m_var.y8; }
  inline if_clock_type& z8()
    { return m_var.z8; }
  inline if_clock_type& x9()
    { return m_var.x9; }
  inline if_clock_type& y9()
    { return m_var.y9; }
  inline if_clock_type& z9()
    { return m_var.z9; }
  inline if_clock_type& x10()
    { return m_var.x10; }
  inline if_clock_type& y10()
    { return m_var.y10; }
  inline if_clock_type& z10()
    { return m_var.z10; }
  inline if_sigs_type& sig1()
    { return m_var.sig1; }
  inline if_sigs_type& sig2()
    { return m_var.sig2; }
  inline if_sigs_type& sig3()
    { return m_var.sig3; }
  inline if_sigs_type& sig4()
    { return m_var.sig4; }
  inline if_sigs_type& sig5()
    { return m_var.sig5; }
  inline if_sigs_type& sig6()
    { return m_var.sig6; }
  inline if_sigs_type& sig7()
    { return m_var.sig7; }
  inline if_sigs_type& sig8()
    { return m_var.sig8; }
  inline if_sigs_type& sig9()
    { return m_var.sig9; }
  inline if_sigs_type& sig10()
    { return m_var.sig10; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_boolean_type& inls()
    { return m_var.inls; }
  

public:
  if_bridge_par_type m_par;  /* parameters */ 
  if_bridge_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;
  int listype;

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();
  inline void _init_1e_fire();
  inline void _init_1f_fire();
  inline void _init_1g_fire();
  inline void _init_1h_fire();
  inline void _init_1i_fire();
  inline void _init_1j_fire();
  inline void _init_1k_fire();
  inline void _init_1l_fire();
  inline void _init_1m_fire();
  inline void _init_1n_fire();
  inline void _init_1o_fire();
  inline void _init_1p_fire();
  inline void _init_1q_fire();
  inline void _init_1r_fire();
  inline void _init_1s_fire();
  inline void _init_1t_fire();
  inline void _init_1u_fire();
  inline void _init_1v_fire();
  inline void _init_1w_fire();
  inline void _init_1x_fire();
  inline void _init_1y_fire();
  inline void _init_1z_fire();
  inline void _init_1ab_fire();
  inline void _init_1bb_fire();
  inline void _init_1cb_fire();
  inline void _init_1db_fire();
  inline void _init_1eb_fire();
  inline void _init_1fb_fire();
  inline void _init_1gb_fire();
  inline void _init_1hb_fire();
  inline void _init_1ib_fire();
  inline void _init_1jb_fire();
  inline void _init_1kb_fire();
  inline void _init_1lb_fire();
  inline void _init_1mb_fire();
  inline void _init_1nb_fire();
  inline void _init_1ob_fire();
  inline void _init_1pb_fire();


         void _mainstate_dispatch(IfMessage*);

         void _mainstate_1_fire(IfMessage*);
  inline void _mainstate_1a_fire();
  inline void _mainstate_1b_fire();
  inline void _mainstate_1c_fire();

         void _mainstate_2_fire(IfMessage*);
  inline void _mainstate_2a_fire();
  inline void _mainstate_2b_fire();
  inline void _mainstate_2c_fire();

         void _mainstate_3_fire(IfMessage*);
  inline void _mainstate_3a_fire();
  inline void _mainstate_3b_fire();
  inline void _mainstate_3c_fire();

         void _mainstate_4_fire(IfMessage*);
  inline void _mainstate_4a_fire();
  inline void _mainstate_4b_fire();
  inline void _mainstate_4c_fire();

         void _mainstate_5_fire(IfMessage*);
  inline void _mainstate_5a_fire();
  inline void _mainstate_5b_fire();
  inline void _mainstate_5c_fire();

         void _mainstate_6_fire(IfMessage*);
  inline void _mainstate_6a_fire();
  inline void _mainstate_6b_fire();
  inline void _mainstate_6c_fire();

         void _mainstate_7_fire(IfMessage*);
  inline void _mainstate_7a_fire();
  inline void _mainstate_7b_fire();
  inline void _mainstate_7c_fire();

         void _mainstate_8_fire(IfMessage*);
  inline void _mainstate_8a_fire();
  inline void _mainstate_8b_fire();
  inline void _mainstate_8c_fire();

         void _mainstate_9_fire(IfMessage*);
  inline void _mainstate_9a_fire();
  inline void _mainstate_9b_fire();
  inline void _mainstate_9c_fire();

         void _mainstate_10_fire(IfMessage*);
  inline void _mainstate_10a_fire();
  inline void _mainstate_10b_fire();
  inline void _mainstate_10c_fire();

         void _mainstate_11_fire(IfMessage*);
  inline void _mainstate_11a_fire();
  inline void _mainstate_11b_fire();
  inline void _mainstate_11c_fire();

         void _mainstate_12_fire(IfMessage*);
  inline void _mainstate_12a_fire();
  inline void _mainstate_12b_fire();
  inline void _mainstate_12c_fire();

         void _mainstate_13_fire(IfMessage*);
  inline void _mainstate_13a_fire();
  inline void _mainstate_13b_fire();
  inline void _mainstate_13c_fire();

         void _mainstate_14_fire(IfMessage*);
  inline void _mainstate_14a_fire();
  inline void _mainstate_14b_fire();
  inline void _mainstate_14c_fire();

         void _mainstate_15_fire(IfMessage*);
  inline void _mainstate_15a_fire();
  inline void _mainstate_15b_fire();
  inline void _mainstate_15c_fire();

         void _mainstate_16_fire(IfMessage*);
  inline void _mainstate_16a_fire();
  inline void _mainstate_16b_fire();
  inline void _mainstate_16c_fire();

         void _mainstate_17_fire(IfMessage*);
  inline void _mainstate_17a_fire();
  inline void _mainstate_17b_fire();
  inline void _mainstate_17c_fire();

         void _mainstate_18_fire(IfMessage*);
  inline void _mainstate_18a_fire();
  inline void _mainstate_18b_fire();
  inline void _mainstate_18c_fire();

         void _mainstate_19_fire(IfMessage*);
  inline void _mainstate_19a_fire();
  inline void _mainstate_19b_fire();
  inline void _mainstate_19c_fire();

         void _mainstate_20_fire(IfMessage*);
  inline void _mainstate_20a_fire();
  inline void _mainstate_20b_fire();
  inline void _mainstate_20c_fire();

         void _mainstate_21_fire(IfMessage*);
  inline void _mainstate_21a_fire();
  inline void _mainstate_21b_fire();
  inline void _mainstate_21c_fire();

         void _mainstate_22_fire(IfMessage*);
  inline void _mainstate_22a_fire();
  inline void _mainstate_22b_fire();
  inline void _mainstate_22c_fire();

         void _mainstate_23_fire(IfMessage*);
  inline void _mainstate_23a_fire();
  inline void _mainstate_23b_fire();
  inline void _mainstate_23c_fire();

         void _mainstate_24_fire(IfMessage*);
  inline void _mainstate_24a_fire();
  inline void _mainstate_24b_fire();
  inline void _mainstate_24c_fire();

         void _mainstate_25_fire(IfMessage*);
  inline void _mainstate_25a_fire();
  inline void _mainstate_25b_fire();
  inline void _mainstate_25c_fire();

         void _mainstate_26_fire(IfMessage*);
  inline void _mainstate_26a_fire();
  inline void _mainstate_26b_fire();
  inline void _mainstate_26c_fire();

         void _mainstate_27_fire(IfMessage*);
  inline void _mainstate_27a_fire();
  inline void _mainstate_27b_fire();
  inline void _mainstate_27c_fire();

         void _mainstate_28_fire(IfMessage*);
  inline void _mainstate_28a_fire();
  inline void _mainstate_28b_fire();
  inline void _mainstate_28c_fire();

         void _mainstate_29_fire(IfMessage*);
  inline void _mainstate_29a_fire();
  inline void _mainstate_29b_fire();
  inline void _mainstate_29c_fire();

         void _mainstate_30_fire(IfMessage*);
  inline void _mainstate_30a_fire();
  inline void _mainstate_30b_fire();
  inline void _mainstate_30c_fire();

         void _mainstate_31_fire(IfMessage*);
  inline void _mainstate_31a_fire();
  inline void _mainstate_31b_fire();
  inline void _mainstate_31c_fire();

         void _mainstate_32_fire(IfMessage*);
  inline void _mainstate_32a_fire();
  inline void _mainstate_32b_fire();
  inline void _mainstate_32c_fire();

         void _mainstate_33_fire(IfMessage*);
  inline void _mainstate_33a_fire();
  inline void _mainstate_33b_fire();
  inline void _mainstate_33c_fire();

         void _mainstate_34_fire(IfMessage*);
  inline void _mainstate_34a_fire();
  inline void _mainstate_34b_fire();
  inline void _mainstate_34c_fire();

         void _mainstate_35_fire(IfMessage*);
  inline void _mainstate_35a_fire();
  inline void _mainstate_35b_fire();
  inline void _mainstate_35c_fire();

         void _mainstate_36_fire(IfMessage*);
  inline void _mainstate_36a_fire();
  inline void _mainstate_36b_fire();
  inline void _mainstate_36c_fire();

         void _mainstate_37_fire(IfMessage*);
  inline void _mainstate_37a_fire();
  inline void _mainstate_37b_fire();
  inline void _mainstate_37c_fire();

         void _mainstate_38_fire(IfMessage*);
  inline void _mainstate_38a_fire();
  inline void _mainstate_38b_fire();
  inline void _mainstate_38c_fire();

         void _mainstate_39_fire(IfMessage*);
  inline void _mainstate_39a_fire();
  inline void _mainstate_39b_fire();
  inline void _mainstate_39c_fire();

         void _mainstate_40_fire(IfMessage*);
  inline void _mainstate_40a_fire();
  inline void _mainstate_40b_fire();
  inline void _mainstate_40c_fire();

         void _mainstate_41_fire(IfMessage*);
  inline void _mainstate_41a_fire();
  inline void _mainstate_41b_fire();
  inline void _mainstate_41c_fire();

         void _mainstate_42_fire(IfMessage*);
  inline void _mainstate_42a_fire();
  inline void _mainstate_42b_fire();
  inline void _mainstate_42c_fire();

         void _mainstate_43_fire(IfMessage*);
  inline void _mainstate_43a_fire();
  inline void _mainstate_43b_fire();
  inline void _mainstate_43c_fire();

         void _mainstate_44_fire(IfMessage*);
  inline void _mainstate_44a_fire();
  inline void _mainstate_44b_fire();
  inline void _mainstate_44c_fire();

         void _mainstate_45_fire(IfMessage*);
  inline void _mainstate_45a_fire();
  inline void _mainstate_45b_fire();
  inline void _mainstate_45c_fire();

         void _mainstate_46_fire(IfMessage*);
  inline void _mainstate_46a_fire();
  inline void _mainstate_46b_fire();
  inline void _mainstate_46c_fire();

         void _mainstate_47_fire(IfMessage*);
  inline void _mainstate_47a_fire();
  inline void _mainstate_47b_fire();
  inline void _mainstate_47c_fire();

         void _mainstate_48_fire(IfMessage*);
  inline void _mainstate_48a_fire();
  inline void _mainstate_48b_fire();
  inline void _mainstate_48c_fire();

         void _mainstate_49_fire(IfMessage*);
  inline void _mainstate_49a_fire();
  inline void _mainstate_49b_fire();
  inline void _mainstate_49c_fire();

         void _mainstate_50_fire(IfMessage*);
  inline void _mainstate_50a_fire();
  inline void _mainstate_50b_fire();
  inline void _mainstate_50c_fire();

         void _mainstate_51_fire(IfMessage*);
  inline void _mainstate_51a_fire();
  inline void _mainstate_51b_fire();
  inline void _mainstate_51c_fire();

         void _mainstate_52_fire(IfMessage*);
  inline void _mainstate_52a_fire();
  inline void _mainstate_52b_fire();
  inline void _mainstate_52c_fire();

         void _mainstate_53_fire(IfMessage*);
  inline void _mainstate_53a_fire();
  inline void _mainstate_53b_fire();
  inline void _mainstate_53c_fire();

         void _mainstate_54_fire(IfMessage*);
  inline void _mainstate_54a_fire();
  inline void _mainstate_54b_fire();
  inline void _mainstate_54c_fire();

         void _mainstate_55_fire(IfMessage*);
  inline void _mainstate_55a_fire();
  inline void _mainstate_55b_fire();
  inline void _mainstate_55c_fire();

         void _mainstate_56_fire(IfMessage*);
  inline void _mainstate_56a_fire();
  inline void _mainstate_56b_fire();
  inline void _mainstate_56c_fire();

         void _mainstate_57_fire(IfMessage*);
  inline void _mainstate_57a_fire();
  inline void _mainstate_57b_fire();
  inline void _mainstate_57c_fire();

         void _mainstate_58_fire(IfMessage*);
  inline void _mainstate_58a_fire();
  inline void _mainstate_58b_fire();
  inline void _mainstate_58c_fire();

         void _mainstate_59_fire(IfMessage*);
  inline void _mainstate_59a_fire();
  inline void _mainstate_59b_fire();
  inline void _mainstate_59c_fire();

         void _mainstate_60_fire(IfMessage*);
  inline void _mainstate_60a_fire();
  inline void _mainstate_60b_fire();
  inline void _mainstate_60c_fire();

         void _mainstate_61_fire(IfMessage*);
  inline void _mainstate_61a_fire();
  inline void _mainstate_61b_fire();
  inline void _mainstate_61c_fire();
  inline void _mainstate_61d_fire();

         void _mainstate_62_fire(IfMessage*);
  inline void _mainstate_62a_fire();
  inline void _mainstate_62b_fire();
  inline void _mainstate_62c_fire();

         void _mainstate_63_fire(IfMessage*);
  inline void _mainstate_63a_fire();
  inline void _mainstate_63b_fire();
  inline void _mainstate_63c_fire();
  inline void _mainstate_63d_fire();
  inline void _mainstate_63e_fire();
  inline void _mainstate_63f_fire();
  inline void _mainstate_63g_fire();
  inline void _mainstate_63h_fire();
  inline void _mainstate_63i_fire();
  inline void _mainstate_63j_fire();
  inline void _mainstate_63k_fire();
  inline void _mainstate_63l_fire();
  inline void _mainstate_63m_fire();
  inline void _mainstate_63n_fire();
  inline void _mainstate_63o_fire();
  inline void _mainstate_63p_fire();
  inline void _mainstate_63q_fire();
  inline void _mainstate_63r_fire();
  inline void _mainstate_63s_fire();
  inline void _mainstate_63t_fire();
  inline void _mainstate_63u_fire();
  inline void _mainstate_63v_fire();
  inline void _mainstate_63w_fire();
  inline void _mainstate_63x_fire();
  inline void _mainstate_63y_fire();
  inline void _mainstate_63z_fire();
  inline void _mainstate_63ab_fire();
  inline void _mainstate_63bb_fire();
  inline void _mainstate_63cb_fire();
  inline void _mainstate_63db_fire();
  inline void _mainstate_63eb_fire();
  inline void _mainstate_63fb_fire();
  inline void _mainstate_63gb_fire();
  inline void _mainstate_63hb_fire();
  inline void _mainstate_63ib_fire();
  inline void _mainstate_63jb_fire();
  inline void _mainstate_63kb_fire();
  inline void _mainstate_63lb_fire();
  inline void _mainstate_63mb_fire();
  inline void _mainstate_63nb_fire();
  inline void _mainstate_63ob_fire();
  inline void _mainstate_63pb_fire();
  inline void _mainstate_63qb_fire();

         void _mainstate_64_fire(IfMessage*);
  inline void _mainstate_64a_fire();
  inline void _mainstate_64b_fire();
  inline void _mainstate_64c_fire();
  inline void _mainstate_64d_fire();
  inline void _mainstate_64e_fire();
  inline void _mainstate_64f_fire();
  inline void _mainstate_64g_fire();
  inline void _mainstate_64h_fire();
  inline void _mainstate_64i_fire();
  inline void _mainstate_64j_fire();
  inline void _mainstate_64k_fire();
  inline void _mainstate_64l_fire();
  inline void _mainstate_64m_fire();
  inline void _mainstate_64n_fire();
  inline void _mainstate_64o_fire();
  inline void _mainstate_64p_fire();
  inline void _mainstate_64q_fire();
  inline void _mainstate_64r_fire();
  inline void _mainstate_64s_fire();
  inline void _mainstate_64t_fire();
  inline void _mainstate_64u_fire();
  inline void _mainstate_64v_fire();
  inline void _mainstate_64w_fire();
  inline void _mainstate_64x_fire();
  inline void _mainstate_64y_fire();
  inline void _mainstate_64z_fire();
  inline void _mainstate_64ab_fire();
  inline void _mainstate_64bb_fire();
  inline void _mainstate_64cb_fire();
  inline void _mainstate_64db_fire();
  inline void _mainstate_64eb_fire();
  inline void _mainstate_64fb_fire();
  inline void _mainstate_64gb_fire();
  inline void _mainstate_64hb_fire();
  inline void _mainstate_64ib_fire();
  inline void _mainstate_64jb_fire();
  inline void _mainstate_64kb_fire();
  inline void _mainstate_64lb_fire();
  inline void _mainstate_64mb_fire();
  inline void _mainstate_64nb_fire();
  inline void _mainstate_64ob_fire();
  inline void _mainstate_64pb_fire();
  inline void _mainstate_64qb_fire();

         void _mainstate_65_fire(IfMessage*);
  inline void _mainstate_65a_fire();
  inline void _mainstate_65b_fire();
  inline void _mainstate_65c_fire();
  inline void _mainstate_65d_fire();
  inline void _mainstate_65e_fire();
  inline void _mainstate_65f_fire();
  inline void _mainstate_65g_fire();
  inline void _mainstate_65h_fire();
  inline void _mainstate_65i_fire();
  inline void _mainstate_65j_fire();
  inline void _mainstate_65k_fire();
  inline void _mainstate_65l_fire();
  inline void _mainstate_65m_fire();
  inline void _mainstate_65n_fire();
  inline void _mainstate_65o_fire();
  inline void _mainstate_65p_fire();
  inline void _mainstate_65q_fire();
  inline void _mainstate_65r_fire();
  inline void _mainstate_65s_fire();
  inline void _mainstate_65t_fire();
  inline void _mainstate_65u_fire();
  inline void _mainstate_65v_fire();
  inline void _mainstate_65w_fire();
  inline void _mainstate_65x_fire();
  inline void _mainstate_65y_fire();
  inline void _mainstate_65z_fire();
  inline void _mainstate_65ab_fire();
  inline void _mainstate_65bb_fire();
  inline void _mainstate_65cb_fire();
  inline void _mainstate_65db_fire();
  inline void _mainstate_65eb_fire();
  inline void _mainstate_65fb_fire();
  inline void _mainstate_65gb_fire();
  inline void _mainstate_65hb_fire();
  inline void _mainstate_65ib_fire();
  inline void _mainstate_65jb_fire();
  inline void _mainstate_65kb_fire();
  inline void _mainstate_65lb_fire();
  inline void _mainstate_65mb_fire();
  inline void _mainstate_65nb_fire();
  inline void _mainstate_65ob_fire();
  inline void _mainstate_65pb_fire();
  inline void _mainstate_65qb_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_bridge_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_twosignal_iterator : public IfIterator {

  public:
    if_twosignal_iterator();
  
  public:
    virtual IfConfig* start();

};


