


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









/* 
 * mytoken [system] instance interface 
 *
 */

class if_mytoken_instance : public IfInstance { 

  public:
    if_mytoken_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_mytoken_instance(const if_mytoken_instance&);

  protected:

};


/* 
 * bridge instance interface 
 *
 */
 
#define if_bridge_process 1



typedef if_void_type if_bridge_par_type;

#define if_bridge_par_copy(x,y) if_void_copy(x,y)
#define if_bridge_par_compare(x,y) if_void_compare(x,y)
#define if_bridge_par_print(x,f) if_void_print(x,f)
#define if_bridge_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_bridge_par_reset(x) if_void_reset(x)
#define if_bridge_par_iterate(x) if_void_iterate(x)
#define if_bridge_par_eq(x,y) if_bridge_par_compare(x,y)==0
#define if_bridge_par_ne(x,y) if_bridge_par_compare(x,y)!=0


typedef struct {
  if_clock_type x;
  if_clock_type F;
  if_clock_type Q;
  if_boolean_type token;
  if_boolean_type inls;
} if_bridge_var_type;

inline void if_bridge_var_copy
    (if_bridge_var_type& x, const if_bridge_var_type y);
inline int if_bridge_var_compare
    (const if_bridge_var_type x, const if_bridge_var_type y);
inline void if_bridge_var_print
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_xmlize
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_reset
    (if_bridge_var_type& x);
#define if_bridge_var_iterate(x)\
  if_clock_iterate(x.x)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_boolean_iterate(x.token)\
  if_boolean_iterate(x.inls)
inline if_bridge_var_type if_bridge_var_make
    (if_clock_type x,if_clock_type F,if_clock_type Q,if_boolean_type token,if_boolean_type inls);
#define if_bridge_var_eq(x,y) if_bridge_var_compare(x,y)==0
#define if_bridge_var_ne(x,y) if_bridge_var_compare(x,y)!=0

class if_bridge_instance : public if_mytoken_instance {

public:
  
  if_bridge_instance();
  if_bridge_instance(const if_bridge_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& x()
    { return m_var.x; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_boolean_type& token()
    { return m_var.token; }
  inline if_boolean_type& inls()
    { return m_var.inls; }
  

public:
  if_bridge_par_type m_par;  /* parameters */ 
  if_bridge_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;
  int listype;

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();
  inline void _init_1e_fire();


         void _execution_dispatch(IfMessage*);

         void _execution_1_fire(IfMessage*);
  inline void _execution_1a_fire();
  inline void _execution_1b_fire();
  inline void _execution_1c_fire();
  inline void _execution_1d_fire();

         void _execution_2_fire(IfMessage*);
  inline void _execution_2a_fire();
  inline void _execution_2b_fire();
  inline void _execution_2c_fire();
  inline void _execution_2d_fire();

         void _execution_3_fire(IfMessage*);
  inline void _execution_3a_fire();
  inline void _execution_3b_fire();
  inline void _execution_3c_fire();
  inline void _execution_3d_fire();
  inline void _execution_3e_fire();

         void _execution_4_fire(IfMessage*);
  inline void _execution_4a_fire();
  inline void _execution_4b_fire();
  inline void _execution_4c_fire();
  inline void _execution_4d_fire();
  inline void _execution_4e_fire();
  inline void _execution_4f_fire();
  inline void _execution_4g_fire();

         void _execution_5_fire(IfMessage*);
  inline void _execution_5a_fire();
  inline void _execution_5b_fire();
  inline void _execution_5c_fire();
  inline void _execution_5d_fire();
  inline void _execution_5e_fire();
  inline void _execution_5f_fire();
  inline void _execution_5g_fire();


         void _suspend_dispatch(IfMessage*);

         void _suspend_1_fire(IfMessage*);
  inline void _suspend_1a_fire();
  inline void _suspend_1b_fire();

         void _suspend_2_fire(IfMessage*);
  inline void _suspend_2a_fire();
  inline void _suspend_2b_fire();

         void _suspend_3_fire(IfMessage*);
  inline void _suspend_3a_fire();
  inline void _suspend_3b_fire();
  inline void _suspend_3c_fire();

         void _suspend_4_fire(IfMessage*);
  inline void _suspend_4a_fire();
  inline void _suspend_4b_fire();
  inline void _suspend_4c_fire();

         void _suspend_5_fire(IfMessage*);
  inline void _suspend_5a_fire();
  inline void _suspend_5b_fire();
  inline void _suspend_5c_fire();
  inline void _suspend_5d_fire();
  inline void _suspend_5e_fire();

         void _suspend_6_fire(IfMessage*);
  inline void _suspend_6a_fire();
  inline void _suspend_6b_fire();
  inline void _suspend_6c_fire();
  inline void _suspend_6d_fire();
  inline void _suspend_6e_fire();

         void _suspend_7_fire(IfMessage*);
  inline void _suspend_7a_fire();
  inline void _suspend_7b_fire();
  inline void _suspend_7c_fire();
  inline void _suspend_7d_fire();
  inline void _suspend_7e_fire();

         void _suspend_8_fire(IfMessage*);
  inline void _suspend_8a_fire();
  inline void _suspend_8b_fire();
  inline void _suspend_8c_fire();
  inline void _suspend_8d_fire();
  inline void _suspend_8e_fire();
  inline void _suspend_8f_fire();
  inline void _suspend_8g_fire();

         void _suspend_9_fire(IfMessage*);
  inline void _suspend_9a_fire();
  inline void _suspend_9b_fire();
  inline void _suspend_9c_fire();
  inline void _suspend_9d_fire();
  inline void _suspend_9e_fire();
  inline void _suspend_9f_fire();
  inline void _suspend_9g_fire();


         void _waiting_dispatch(IfMessage*);

         void _waiting_1_fire(IfMessage*);
  inline void _waiting_1a_fire();
  inline void _waiting_1b_fire();
  inline void _waiting_1c_fire();

         void _waiting_2_fire(IfMessage*);
  inline void _waiting_2a_fire();
  inline void _waiting_2b_fire();
  inline void _waiting_2c_fire();

         void _waiting_3_fire(IfMessage*);
  inline void _waiting_3a_fire();
  inline void _waiting_3b_fire();
  inline void _waiting_3c_fire();
  inline void _waiting_3d_fire();

         void _waiting_4_fire(IfMessage*);
  inline void _waiting_4a_fire();
  inline void _waiting_4b_fire();
  inline void _waiting_4c_fire();
  inline void _waiting_4d_fire();
  inline void _waiting_4e_fire();

         void _waiting_5_fire(IfMessage*);
  inline void _waiting_5a_fire();
  inline void _waiting_5b_fire();
  inline void _waiting_5c_fire();
  inline void _waiting_5d_fire();
  inline void _waiting_5e_fire();
  inline void _waiting_5f_fire();
  inline void _waiting_5g_fire();

         void _waiting_6_fire(IfMessage*);
  inline void _waiting_6a_fire();
  inline void _waiting_6b_fire();
  inline void _waiting_6c_fire();
  inline void _waiting_6d_fire();
  inline void _waiting_6e_fire();
  inline void _waiting_6f_fire();
  inline void _waiting_6g_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_bridge_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_mytoken_iterator : public IfIterator {

  public:
    if_mytoken_iterator();
  
  public:
    virtual IfConfig* start();

};


