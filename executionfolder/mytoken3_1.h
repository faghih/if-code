


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









/* 
 * mytoken [system] instance interface 
 *
 */

class if_mytoken_instance : public IfInstance { 

  public:
    if_mytoken_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_mytoken_instance(const if_mytoken_instance&);

  protected:

};


/* 
 * bridge instance interface 
 *
 */
 
#define if_bridge_process 1



typedef if_void_type if_bridge_par_type;

#define if_bridge_par_copy(x,y) if_void_copy(x,y)
#define if_bridge_par_compare(x,y) if_void_compare(x,y)
#define if_bridge_par_print(x,f) if_void_print(x,f)
#define if_bridge_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_bridge_par_reset(x) if_void_reset(x)
#define if_bridge_par_iterate(x) if_void_iterate(x)
#define if_bridge_par_eq(x,y) if_bridge_par_compare(x,y)==0
#define if_bridge_par_ne(x,y) if_bridge_par_compare(x,y)!=0


typedef struct {
  if_clock_type C;
  if_clock_type F;
  if_clock_type Q;
  if_boolean_type token;
} if_bridge_var_type;

inline void if_bridge_var_copy
    (if_bridge_var_type& x, const if_bridge_var_type y);
inline int if_bridge_var_compare
    (const if_bridge_var_type x, const if_bridge_var_type y);
inline void if_bridge_var_print
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_xmlize
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_reset
    (if_bridge_var_type& x);
#define if_bridge_var_iterate(x)\
  if_clock_iterate(x.C)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_boolean_iterate(x.token)
inline if_bridge_var_type if_bridge_var_make
    (if_clock_type C,if_clock_type F,if_clock_type Q,if_boolean_type token);
#define if_bridge_var_eq(x,y) if_bridge_var_compare(x,y)==0
#define if_bridge_var_ne(x,y) if_bridge_var_compare(x,y)!=0

class if_bridge_instance : public if_mytoken_instance {

public:
  
  if_bridge_instance();
  if_bridge_instance(const if_bridge_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& C()
    { return m_var.C; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_boolean_type& token()
    { return m_var.token; }
  

public:
  if_bridge_par_type m_par;  /* parameters */ 
  if_bridge_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;
  int listype;

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();


         void _execution_dispatch(IfMessage*);

         void _execution_1_fire(IfMessage*);
  inline void _execution_1a_fire();
  inline void _execution_1b_fire();
  inline void _execution_1c_fire();
  inline void _execution_1d_fire();
  inline void _execution_1e_fire();
  inline void _execution_1f_fire();

         void _execution_2_fire(IfMessage*);
  inline void _execution_2a_fire();
  inline void _execution_2b_fire();
  inline void _execution_2c_fire();
  inline void _execution_2d_fire();

         void _execution_3_fire(IfMessage*);
  inline void _execution_3a_fire();
  inline void _execution_3b_fire();
  inline void _execution_3c_fire();
  inline void _execution_3d_fire();

         void _execution_4_fire(IfMessage*);
  inline void _execution_4a_fire();
  inline void _execution_4b_fire();
  inline void _execution_4c_fire();

         void _execution_5_fire(IfMessage*);
  inline void _execution_5a_fire();
  inline void _execution_5b_fire();
  inline void _execution_5c_fire();
  inline void _execution_5d_fire();

         void _execution_6_fire(IfMessage*);
  inline void _execution_6a_fire();
  inline void _execution_6b_fire();
  inline void _execution_6c_fire();

         void _execution_7_fire(IfMessage*);
  inline void _execution_7a_fire();
  inline void _execution_7b_fire();
  inline void _execution_7c_fire();

         void _execution_8_fire(IfMessage*);
  inline void _execution_8a_fire();
  inline void _execution_8b_fire();

         void _execution_9_fire(IfMessage*);
  inline void _execution_9a_fire();
  inline void _execution_9b_fire();
  inline void _execution_9c_fire();

         void _execution_10_fire(IfMessage*);
  inline void _execution_10a_fire();
  inline void _execution_10b_fire();

         void _execution_11_fire(IfMessage*);
  inline void _execution_11a_fire();
  inline void _execution_11b_fire();
  inline void _execution_11c_fire();

         void _execution_12_fire(IfMessage*);
  inline void _execution_12a_fire();
  inline void _execution_12b_fire();
  inline void _execution_12c_fire();

         void _execution_13_fire(IfMessage*);
  inline void _execution_13a_fire();
  inline void _execution_13b_fire();
  inline void _execution_13c_fire();
  inline void _execution_13d_fire();
  inline void _execution_13e_fire();
  inline void _execution_13f_fire();

         void _execution_14_fire(IfMessage*);
  inline void _execution_14a_fire();
  inline void _execution_14b_fire();
  inline void _execution_14c_fire();
  inline void _execution_14d_fire();
  inline void _execution_14e_fire();
  inline void _execution_14f_fire();


         void _suspend_dispatch(IfMessage*);

         void _suspend_1_fire(IfMessage*);
  inline void _suspend_1a_fire();
  inline void _suspend_1b_fire();
  inline void _suspend_1c_fire();

         void _suspend_2_fire(IfMessage*);
  inline void _suspend_2a_fire();
  inline void _suspend_2b_fire();
  inline void _suspend_2c_fire();

         void _suspend_3_fire(IfMessage*);
  inline void _suspend_3a_fire();
  inline void _suspend_3b_fire();
  inline void _suspend_3c_fire();

         void _suspend_4_fire(IfMessage*);
  inline void _suspend_4a_fire();
  inline void _suspend_4b_fire();
  inline void _suspend_4c_fire();

         void _suspend_5_fire(IfMessage*);
  inline void _suspend_5a_fire();
  inline void _suspend_5b_fire();
  inline void _suspend_5c_fire();
  inline void _suspend_5d_fire();

         void _suspend_6_fire(IfMessage*);
  inline void _suspend_6a_fire();
  inline void _suspend_6b_fire();
  inline void _suspend_6c_fire();
  inline void _suspend_6d_fire();
  inline void _suspend_6e_fire();
  inline void _suspend_6f_fire();

         void _suspend_7_fire(IfMessage*);
  inline void _suspend_7a_fire();
  inline void _suspend_7b_fire();
  inline void _suspend_7c_fire();
  inline void _suspend_7d_fire();
  inline void _suspend_7e_fire();
  inline void _suspend_7f_fire();

         void _suspend_8_fire(IfMessage*);
  inline void _suspend_8a_fire();
  inline void _suspend_8b_fire();

         void _suspend_9_fire(IfMessage*);
  inline void _suspend_9a_fire();
  inline void _suspend_9b_fire();
  inline void _suspend_9c_fire();

         void _suspend_10_fire(IfMessage*);
  inline void _suspend_10a_fire();
  inline void _suspend_10b_fire();

         void _suspend_11_fire(IfMessage*);
  inline void _suspend_11a_fire();
  inline void _suspend_11b_fire();

         void _suspend_12_fire(IfMessage*);
  inline void _suspend_12a_fire();
  inline void _suspend_12b_fire();
  inline void _suspend_12c_fire();

         void _suspend_13_fire(IfMessage*);
  inline void _suspend_13a_fire();
  inline void _suspend_13b_fire();
  inline void _suspend_13c_fire();

         void _suspend_14_fire(IfMessage*);
  inline void _suspend_14a_fire();
  inline void _suspend_14b_fire();
  inline void _suspend_14c_fire();

         void _suspend_15_fire(IfMessage*);
  inline void _suspend_15a_fire();
  inline void _suspend_15b_fire();
  inline void _suspend_15c_fire();

         void _suspend_16_fire(IfMessage*);
  inline void _suspend_16a_fire();
  inline void _suspend_16b_fire();
  inline void _suspend_16c_fire();
  inline void _suspend_16d_fire();

         void _suspend_17_fire(IfMessage*);
  inline void _suspend_17a_fire();
  inline void _suspend_17b_fire();
  inline void _suspend_17c_fire();
  inline void _suspend_17d_fire();

         void _suspend_18_fire(IfMessage*);
  inline void _suspend_18a_fire();
  inline void _suspend_18b_fire();
  inline void _suspend_18c_fire();

         void _suspend_19_fire(IfMessage*);
  inline void _suspend_19a_fire();
  inline void _suspend_19b_fire();
  inline void _suspend_19c_fire();

         void _suspend_20_fire(IfMessage*);
  inline void _suspend_20a_fire();
  inline void _suspend_20b_fire();
  inline void _suspend_20c_fire();
  inline void _suspend_20d_fire();

         void _suspend_21_fire(IfMessage*);
  inline void _suspend_21a_fire();
  inline void _suspend_21b_fire();
  inline void _suspend_21c_fire();
  inline void _suspend_21d_fire();

         void _suspend_22_fire(IfMessage*);
  inline void _suspend_22a_fire();
  inline void _suspend_22b_fire();
  inline void _suspend_22c_fire();

         void _suspend_23_fire(IfMessage*);
  inline void _suspend_23a_fire();
  inline void _suspend_23b_fire();
  inline void _suspend_23c_fire();

         void _suspend_24_fire(IfMessage*);
  inline void _suspend_24a_fire();
  inline void _suspend_24b_fire();
  inline void _suspend_24c_fire();
  inline void _suspend_24d_fire();

         void _suspend_25_fire(IfMessage*);
  inline void _suspend_25a_fire();
  inline void _suspend_25b_fire();
  inline void _suspend_25c_fire();
  inline void _suspend_25d_fire();


         void _waiting_dispatch(IfMessage*);

         void _waiting_1_fire(IfMessage*);
  inline void _waiting_1a_fire();
  inline void _waiting_1b_fire();
  inline void _waiting_1c_fire();

         void _waiting_2_fire(IfMessage*);
  inline void _waiting_2a_fire();
  inline void _waiting_2b_fire();
  inline void _waiting_2c_fire();
  inline void _waiting_2d_fire();

         void _waiting_3_fire(IfMessage*);
  inline void _waiting_3a_fire();
  inline void _waiting_3b_fire();
  inline void _waiting_3c_fire();

         void _waiting_4_fire(IfMessage*);
  inline void _waiting_4a_fire();
  inline void _waiting_4b_fire();

         void _waiting_5_fire(IfMessage*);
  inline void _waiting_5a_fire();
  inline void _waiting_5b_fire();
  inline void _waiting_5c_fire();

         void _waiting_6_fire(IfMessage*);
  inline void _waiting_6a_fire();
  inline void _waiting_6b_fire();
  inline void _waiting_6c_fire();

         void _waiting_7_fire(IfMessage*);
  inline void _waiting_7a_fire();
  inline void _waiting_7b_fire();

         void _waiting_8_fire(IfMessage*);
  inline void _waiting_8a_fire();
  inline void _waiting_8b_fire();
  inline void _waiting_8c_fire();
  inline void _waiting_8d_fire();

         void _waiting_9_fire(IfMessage*);
  inline void _waiting_9a_fire();
  inline void _waiting_9b_fire();
  inline void _waiting_9c_fire();

         void _waiting_10_fire(IfMessage*);
  inline void _waiting_10a_fire();
  inline void _waiting_10b_fire();
  inline void _waiting_10c_fire();
  inline void _waiting_10d_fire();

         void _waiting_11_fire(IfMessage*);
  inline void _waiting_11a_fire();
  inline void _waiting_11b_fire();
  inline void _waiting_11c_fire();
  inline void _waiting_11d_fire();

         void _waiting_12_fire(IfMessage*);
  inline void _waiting_12a_fire();
  inline void _waiting_12b_fire();
  inline void _waiting_12c_fire();

         void _waiting_13_fire(IfMessage*);
  inline void _waiting_13a_fire();
  inline void _waiting_13b_fire();
  inline void _waiting_13c_fire();

         void _waiting_14_fire(IfMessage*);
  inline void _waiting_14a_fire();
  inline void _waiting_14b_fire();

         void _waiting_15_fire(IfMessage*);
  inline void _waiting_15a_fire();
  inline void _waiting_15b_fire();
  inline void _waiting_15c_fire();

         void _waiting_16_fire(IfMessage*);
  inline void _waiting_16a_fire();
  inline void _waiting_16b_fire();
  inline void _waiting_16c_fire();

         void _waiting_17_fire(IfMessage*);
  inline void _waiting_17a_fire();
  inline void _waiting_17b_fire();

         void _waiting_18_fire(IfMessage*);
  inline void _waiting_18a_fire();
  inline void _waiting_18b_fire();
  inline void _waiting_18c_fire();
  inline void _waiting_18d_fire();

         void _waiting_19_fire(IfMessage*);
  inline void _waiting_19a_fire();
  inline void _waiting_19b_fire();
  inline void _waiting_19c_fire();

         void _waiting_20_fire(IfMessage*);
  inline void _waiting_20a_fire();
  inline void _waiting_20b_fire();
  inline void _waiting_20c_fire();
  inline void _waiting_20d_fire();

         void _waiting_21_fire(IfMessage*);
  inline void _waiting_21a_fire();
  inline void _waiting_21b_fire();
  inline void _waiting_21c_fire();
  inline void _waiting_21d_fire();

         void _waiting_22_fire(IfMessage*);
  inline void _waiting_22a_fire();
  inline void _waiting_22b_fire();
  inline void _waiting_22c_fire();

         void _waiting_23_fire(IfMessage*);
  inline void _waiting_23a_fire();
  inline void _waiting_23b_fire();
  inline void _waiting_23c_fire();

         void _waiting_24_fire(IfMessage*);
  inline void _waiting_24a_fire();
  inline void _waiting_24b_fire();
  inline void _waiting_24c_fire();

         void _waiting_25_fire(IfMessage*);
  inline void _waiting_25a_fire();
  inline void _waiting_25b_fire();
  inline void _waiting_25c_fire();
  inline void _waiting_25d_fire();
  inline void _waiting_25e_fire();
  inline void _waiting_25f_fire();

         void _waiting_26_fire(IfMessage*);
  inline void _waiting_26a_fire();
  inline void _waiting_26b_fire();
  inline void _waiting_26c_fire();
  inline void _waiting_26d_fire();
  inline void _waiting_26e_fire();
  inline void _waiting_26f_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_bridge_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_mytoken_iterator : public IfIterator {

  public:
    if_mytoken_iterator();
  
  public:
    virtual IfConfig* start();

};


