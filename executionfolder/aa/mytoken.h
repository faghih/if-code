


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









/* 
 * mytoken [system] instance interface 
 *
 */

class if_mytoken_instance : public IfInstance { 

  public:
    if_mytoken_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_mytoken_instance(const if_mytoken_instance&);



  protected:

};


/* 
 * exewait instance interface 
 *
 */
 
#define if_exewait_process 1



typedef if_void_type if_exewait_par_type;

#define if_exewait_par_copy(x,y) if_void_copy(x,y)
#define if_exewait_par_compare(x,y) if_void_compare(x,y)
#define if_exewait_par_print(x,f) if_void_print(x,f)
#define if_exewait_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_exewait_par_reset(x) if_void_reset(x)
#define if_exewait_par_iterate(x) if_void_iterate(x)
#define if_exewait_par_eq(x,y) if_exewait_par_compare(x,y)==0
#define if_exewait_par_ne(x,y) if_exewait_par_compare(x,y)!=0


typedef struct {
  if_clock_type E;
  if_clock_type W;
  if_clock_type F;
  if_clock_type Q;
  if_boolean_type token;
} if_exewait_var_type;

inline void if_exewait_var_copy
    (if_exewait_var_type& x, const if_exewait_var_type y);
inline int if_exewait_var_compare
    (const if_exewait_var_type x, const if_exewait_var_type y);
inline void if_exewait_var_print
    (const if_exewait_var_type x, FILE* f);
inline void if_exewait_var_xmlize
    (const if_exewait_var_type x, FILE* f);
inline void if_exewait_var_reset
    (if_exewait_var_type& x);
#define if_exewait_var_iterate(x)\
  if_clock_iterate(x.E)\
  if_clock_iterate(x.W)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_boolean_iterate(x.token)
inline if_exewait_var_type if_exewait_var_make
    (if_clock_type E,if_clock_type W,if_clock_type F,if_clock_type Q,if_boolean_type token);
#define if_exewait_var_eq(x,y) if_exewait_var_compare(x,y)==0
#define if_exewait_var_ne(x,y) if_exewait_var_compare(x,y)!=0

class if_exewait_instance : public if_mytoken_instance {

//protected:
  //  unsigned vertex_num;

/*public:
    inline unsigned getVertex() const
    { return vertex_num; }
  inline void setVertex(const unsigned vertex)
    { vertex_num = vertex; }
*/
public:
  
  if_exewait_instance();
  if_exewait_instance(const if_exewait_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& E()
    { return m_var.E; }
  inline if_clock_type& W()
    { return m_var.W; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_boolean_type& token()
    { return m_var.token; }
  

public:
  if_exewait_par_type m_par;  /* parameters */ 
  if_exewait_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();
  inline void _init_1e_fire();


         void _execution_dispatch(IfMessage*);

         void _execution_1_fire(IfMessage*);
  inline void _execution_1a_fire();
  inline void _execution_1b_fire();
  inline void _execution_1c_fire();
  inline void _execution_1d_fire();
  inline void _execution_1e_fire();
  inline void _execution_1f_fire();

         void _execution_2_fire(IfMessage*);
  inline void _execution_2a_fire();
  inline void _execution_2b_fire();
  inline void _execution_2c_fire();
  inline void _execution_2d_fire();

         void _execution_3_fire(IfMessage*);
  inline void _execution_3a_fire();
  inline void _execution_3b_fire();
  inline void _execution_3c_fire();

         void _execution_4_fire(IfMessage*);
  inline void _execution_4a_fire();
  inline void _execution_4b_fire();
  inline void _execution_4c_fire();


         void _waiting_dispatch(IfMessage*);

         void _waiting_1_fire(IfMessage*);
  inline void _waiting_1a_fire();
  inline void _waiting_1b_fire();
  inline void _waiting_1c_fire();

         void _waiting_2_fire(IfMessage*);
  inline void _waiting_2a_fire();
  inline void _waiting_2b_fire();
  inline void _waiting_2c_fire();

         void _waiting_3_fire(IfMessage*);
  inline void _waiting_3a_fire();
  inline void _waiting_3b_fire();
  inline void _waiting_3c_fire();

         void _waiting_4_fire(IfMessage*);
  inline void _waiting_4a_fire();
  inline void _waiting_4b_fire();
  inline void _waiting_4c_fire();

         void _waiting_5_fire(IfMessage*);
  inline void _waiting_5a_fire();
  inline void _waiting_5b_fire();
  inline void _waiting_5c_fire();
  inline void _waiting_5d_fire();

         void _waiting_6_fire(IfMessage*);
  inline void _waiting_6a_fire();
  inline void _waiting_6b_fire();
  inline void _waiting_6c_fire();
  inline void _waiting_6d_fire();

         void _waiting_7_fire(IfMessage*);
  inline void _waiting_7a_fire();
  inline void _waiting_7b_fire();
  inline void _waiting_7c_fire();
  inline void _waiting_7d_fire();
  inline void _waiting_7e_fire();
  inline void _waiting_7f_fire();

         void _waiting_8_fire(IfMessage*);
  inline void _waiting_8a_fire();
  inline void _waiting_8b_fire();
  inline void _waiting_8c_fire();
  inline void _waiting_8d_fire();
  inline void _waiting_8e_fire();
  inline void _waiting_8f_fire();

         void _waiting_9_fire(IfMessage*);
  inline void _waiting_9a_fire();
  inline void _waiting_9b_fire();
  inline void _waiting_9c_fire();
  inline void _waiting_9d_fire();
  inline void _waiting_9e_fire();
  inline void _waiting_9f_fire();

         void _waiting_10_fire(IfMessage*);
  inline void _waiting_10a_fire();
  inline void _waiting_10b_fire();
  inline void _waiting_10c_fire();
  inline void _waiting_10d_fire();

         void _waiting_11_fire(IfMessage*);
  inline void _waiting_11a_fire();
  inline void _waiting_11b_fire();
  inline void _waiting_11c_fire();

         void _waiting_12_fire(IfMessage*);
  inline void _waiting_12a_fire();
  inline void _waiting_12b_fire();
  inline void _waiting_12c_fire();
  inline void _waiting_12d_fire();

         void _waiting_13_fire(IfMessage*);
  inline void _waiting_13a_fire();
  inline void _waiting_13b_fire();
  inline void _waiting_13c_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_exewait_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_mytoken_iterator : public IfIterator {

  public:
    if_mytoken_iterator();
  
  public:
    virtual IfConfig* start();

};


