


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









typedef enum {
  if_G_constant,
  if_Y_constant,
  if_R_constant
} if_sigs_type;

extern const char* if_sigs_name[];

#define if_sigs_copy(x,y) (x)=(y)
#define if_sigs_compare(x,y) (x)-(y)
#define if_sigs_print(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_xmlize(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_reset(x) (x)=(if_sigs_type)0
#define if_sigs_iterate(x) for(x=(if_sigs_type)0;x<=if_R_constant;x=(if_sigs_type)(x + 1))
#define if_sigs_eq(x,y) if_sigs_compare(x,y)==0
#define if_sigs_ne(x,y) if_sigs_compare(x,y)!=0


/* 
 * twosignal [system] instance interface 
 *
 */

class if_twosignal_instance : public IfInstance { 

  public:
    if_twosignal_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_twosignal_instance(const if_twosignal_instance&);

  protected:

};


/* 
 * bridge instance interface 
 *
 */
 
#define if_bridge_process 1



typedef if_void_type if_bridge_par_type;

#define if_bridge_par_copy(x,y) if_void_copy(x,y)
#define if_bridge_par_compare(x,y) if_void_compare(x,y)
#define if_bridge_par_print(x,f) if_void_print(x,f)
#define if_bridge_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_bridge_par_reset(x) if_void_reset(x)
#define if_bridge_par_iterate(x) if_void_iterate(x)
#define if_bridge_par_eq(x,y) if_bridge_par_compare(x,y)==0
#define if_bridge_par_ne(x,y) if_bridge_par_compare(x,y)!=0


typedef struct {
  if_clock_type x1;
  if_clock_type y1;
  if_clock_type z1;
  if_clock_type x2;
  if_clock_type y2;
  if_clock_type z2;
  if_clock_type F;
  if_clock_type Q;
  if_sigs_type sig1;
  if_sigs_type sig2;
} if_bridge_var_type;

inline void if_bridge_var_copy
    (if_bridge_var_type& x, const if_bridge_var_type y);
inline int if_bridge_var_compare
    (const if_bridge_var_type x, const if_bridge_var_type y);
inline void if_bridge_var_print
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_xmlize
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_reset
    (if_bridge_var_type& x);
#define if_bridge_var_iterate(x)\
  if_clock_iterate(x.x1)\
  if_clock_iterate(x.y1)\
  if_clock_iterate(x.z1)\
  if_clock_iterate(x.x2)\
  if_clock_iterate(x.y2)\
  if_clock_iterate(x.z2)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_sigs_iterate(x.sig1)\
  if_sigs_iterate(x.sig2)
inline if_bridge_var_type if_bridge_var_make
    (if_clock_type x1,if_clock_type y1,if_clock_type z1,if_clock_type x2,if_clock_type y2,if_clock_type z2,if_clock_type F,if_clock_type Q,if_sigs_type sig1,if_sigs_type sig2);
#define if_bridge_var_eq(x,y) if_bridge_var_compare(x,y)==0
#define if_bridge_var_ne(x,y) if_bridge_var_compare(x,y)!=0

class if_bridge_instance : public if_twosignal_instance {

public:
  
  if_bridge_instance();
  if_bridge_instance(const if_bridge_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& x1()
    { return m_var.x1; }
  inline if_clock_type& y1()
    { return m_var.y1; }
  inline if_clock_type& z1()
    { return m_var.z1; }
  inline if_clock_type& x2()
    { return m_var.x2; }
  inline if_clock_type& y2()
    { return m_var.y2; }
  inline if_clock_type& z2()
    { return m_var.z2; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_sigs_type& sig1()
    { return m_var.sig1; }
  inline if_sigs_type& sig2()
    { return m_var.sig2; }
  

public:
  if_bridge_par_type m_par;  /* parameters */ 
  if_bridge_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;
  int listype;

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();
  inline void _init_1e_fire();
  inline void _init_1f_fire();
  inline void _init_1g_fire();
  inline void _init_1h_fire();
  inline void _init_1i_fire();
  inline void _init_1j_fire();


         void _mainstate_dispatch(IfMessage*);

         void _mainstate_1_fire(IfMessage*);
  inline void _mainstate_1a_fire();
  inline void _mainstate_1b_fire();
  inline void _mainstate_1c_fire();

         void _mainstate_2_fire(IfMessage*);
  inline void _mainstate_2a_fire();
  inline void _mainstate_2b_fire();
  inline void _mainstate_2c_fire();

         void _mainstate_3_fire(IfMessage*);
  inline void _mainstate_3a_fire();
  inline void _mainstate_3b_fire();
  inline void _mainstate_3c_fire();

         void _mainstate_4_fire(IfMessage*);
  inline void _mainstate_4a_fire();
  inline void _mainstate_4b_fire();
  inline void _mainstate_4c_fire();

         void _mainstate_5_fire(IfMessage*);
  inline void _mainstate_5a_fire();
  inline void _mainstate_5b_fire();
  inline void _mainstate_5c_fire();

         void _mainstate_6_fire(IfMessage*);
  inline void _mainstate_6a_fire();
  inline void _mainstate_6b_fire();
  inline void _mainstate_6c_fire();

         void _mainstate_7_fire(IfMessage*);
  inline void _mainstate_7a_fire();
  inline void _mainstate_7b_fire();
  inline void _mainstate_7c_fire();

         void _mainstate_8_fire(IfMessage*);
  inline void _mainstate_8a_fire();
  inline void _mainstate_8b_fire();
  inline void _mainstate_8c_fire();

         void _mainstate_9_fire(IfMessage*);
  inline void _mainstate_9a_fire();
  inline void _mainstate_9b_fire();
  inline void _mainstate_9c_fire();
  inline void _mainstate_9d_fire();
  inline void _mainstate_9e_fire();
  inline void _mainstate_9f_fire();
  inline void _mainstate_9g_fire();
  inline void _mainstate_9h_fire();
  inline void _mainstate_9i_fire();
  inline void _mainstate_9j_fire();
  inline void _mainstate_9k_fire();

         void _mainstate_10_fire(IfMessage*);
  inline void _mainstate_10a_fire();
  inline void _mainstate_10b_fire();
  inline void _mainstate_10c_fire();
  inline void _mainstate_10d_fire();
  inline void _mainstate_10e_fire();
  inline void _mainstate_10f_fire();
  inline void _mainstate_10g_fire();
  inline void _mainstate_10h_fire();
  inline void _mainstate_10i_fire();
  inline void _mainstate_10j_fire();
  inline void _mainstate_10k_fire();

         void _mainstate_11_fire(IfMessage*);
  inline void _mainstate_11a_fire();
  inline void _mainstate_11b_fire();
  inline void _mainstate_11c_fire();
  inline void _mainstate_11d_fire();
  inline void _mainstate_11e_fire();
  inline void _mainstate_11f_fire();
  inline void _mainstate_11g_fire();
  inline void _mainstate_11h_fire();
  inline void _mainstate_11i_fire();
  inline void _mainstate_11j_fire();
  inline void _mainstate_11k_fire();

         void _mainstate_12_fire(IfMessage*);
  inline void _mainstate_12a_fire();
  inline void _mainstate_12b_fire();

         void _mainstate_13_fire(IfMessage*);
  inline void _mainstate_13a_fire();
  inline void _mainstate_13b_fire();

         void _mainstate_14_fire(IfMessage*);
  inline void _mainstate_14a_fire();
  inline void _mainstate_14b_fire();

         void _mainstate_15_fire(IfMessage*);
  inline void _mainstate_15a_fire();
  inline void _mainstate_15b_fire();

         void _mainstate_16_fire(IfMessage*);
  inline void _mainstate_16a_fire();
  inline void _mainstate_16b_fire();

         void _mainstate_17_fire(IfMessage*);
  inline void _mainstate_17a_fire();
  inline void _mainstate_17b_fire();

         void _mainstate_18_fire(IfMessage*);
  inline void _mainstate_18a_fire();
  inline void _mainstate_18b_fire();

         void _mainstate_19_fire(IfMessage*);
  inline void _mainstate_19a_fire();
  inline void _mainstate_19b_fire();

         void _mainstate_20_fire(IfMessage*);
  inline void _mainstate_20a_fire();
  inline void _mainstate_20b_fire();

         void _mainstate_21_fire(IfMessage*);
  inline void _mainstate_21a_fire();
  inline void _mainstate_21b_fire();

         void _mainstate_22_fire(IfMessage*);
  inline void _mainstate_22a_fire();
  inline void _mainstate_22b_fire();

         void _mainstate_23_fire(IfMessage*);
  inline void _mainstate_23a_fire();
  inline void _mainstate_23b_fire();

         void _mainstate_24_fire(IfMessage*);
  inline void _mainstate_24a_fire();
  inline void _mainstate_24b_fire();
  inline void _mainstate_24c_fire();
  inline void _mainstate_24d_fire();

         void _mainstate_25_fire(IfMessage*);
  inline void _mainstate_25a_fire();
  inline void _mainstate_25b_fire();
  inline void _mainstate_25c_fire();
  inline void _mainstate_25d_fire();

         void _mainstate_26_fire(IfMessage*);
  inline void _mainstate_26a_fire();
  inline void _mainstate_26b_fire();
  inline void _mainstate_26c_fire();
  inline void _mainstate_26d_fire();

         void _mainstate_27_fire(IfMessage*);
  inline void _mainstate_27a_fire();
  inline void _mainstate_27b_fire();
  inline void _mainstate_27c_fire();
  inline void _mainstate_27d_fire();

         void _mainstate_28_fire(IfMessage*);
  inline void _mainstate_28a_fire();
  inline void _mainstate_28b_fire();
  inline void _mainstate_28c_fire();
  inline void _mainstate_28d_fire();

         void _mainstate_29_fire(IfMessage*);
  inline void _mainstate_29a_fire();
  inline void _mainstate_29b_fire();
  inline void _mainstate_29c_fire();
  inline void _mainstate_29d_fire();

         void _mainstate_30_fire(IfMessage*);
  inline void _mainstate_30a_fire();
  inline void _mainstate_30b_fire();
  inline void _mainstate_30c_fire();
  inline void _mainstate_30d_fire();

         void _mainstate_31_fire(IfMessage*);
  inline void _mainstate_31a_fire();
  inline void _mainstate_31b_fire();
  inline void _mainstate_31c_fire();
  inline void _mainstate_31d_fire();

         void _mainstate_32_fire(IfMessage*);
  inline void _mainstate_32a_fire();
  inline void _mainstate_32b_fire();
  inline void _mainstate_32c_fire();
  inline void _mainstate_32d_fire();

         void _mainstate_33_fire(IfMessage*);
  inline void _mainstate_33a_fire();
  inline void _mainstate_33b_fire();
  inline void _mainstate_33c_fire();
  inline void _mainstate_33d_fire();

         void _mainstate_34_fire(IfMessage*);
  inline void _mainstate_34a_fire();
  inline void _mainstate_34b_fire();
  inline void _mainstate_34c_fire();
  inline void _mainstate_34d_fire();

         void _mainstate_35_fire(IfMessage*);
  inline void _mainstate_35a_fire();
  inline void _mainstate_35b_fire();
  inline void _mainstate_35c_fire();
  inline void _mainstate_35d_fire();

         void _mainstate_36_fire(IfMessage*);
  inline void _mainstate_36a_fire();
  inline void _mainstate_36b_fire();
  inline void _mainstate_36c_fire();
  inline void _mainstate_36d_fire();

         void _mainstate_37_fire(IfMessage*);
  inline void _mainstate_37a_fire();
  inline void _mainstate_37b_fire();
  inline void _mainstate_37c_fire();
  inline void _mainstate_37d_fire();

         void _mainstate_38_fire(IfMessage*);
  inline void _mainstate_38a_fire();
  inline void _mainstate_38b_fire();
  inline void _mainstate_38c_fire();
  inline void _mainstate_38d_fire();

         void _mainstate_39_fire(IfMessage*);
  inline void _mainstate_39a_fire();
  inline void _mainstate_39b_fire();
  inline void _mainstate_39c_fire();
  inline void _mainstate_39d_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_bridge_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_twosignal_iterator : public IfIterator {

  public:
    if_twosignal_iterator();
  
  public:
    virtual IfConfig* start();

};


