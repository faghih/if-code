


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









typedef enum {
  if_G_constant,
  if_Y_constant,
  if_R_constant
} if_sigs_type;

extern const char* if_sigs_name[];

#define if_sigs_copy(x,y) (x)=(y)
#define if_sigs_compare(x,y) (x)-(y)
#define if_sigs_print(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_xmlize(x,f) fprintf(f,"%s",if_sigs_name[x])
#define if_sigs_reset(x) (x)=(if_sigs_type)0
#define if_sigs_iterate(x) for(x=(if_sigs_type)0;x<=if_R_constant;x=(if_sigs_type)(x + 1))
#define if_sigs_eq(x,y) if_sigs_compare(x,y)==0
#define if_sigs_ne(x,y) if_sigs_compare(x,y)!=0


/* 
 * twosignal [system] instance interface 
 *
 */

class if_twosignal_instance : public IfInstance { 

  public:
    if_twosignal_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_twosignal_instance(const if_twosignal_instance&);

  protected:

};


/* 
 * bridge instance interface 
 *
 */
 
#define if_bridge_process 1



typedef if_void_type if_bridge_par_type;

#define if_bridge_par_copy(x,y) if_void_copy(x,y)
#define if_bridge_par_compare(x,y) if_void_compare(x,y)
#define if_bridge_par_print(x,f) if_void_print(x,f)
#define if_bridge_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_bridge_par_reset(x) if_void_reset(x)
#define if_bridge_par_iterate(x) if_void_iterate(x)
#define if_bridge_par_eq(x,y) if_bridge_par_compare(x,y)==0
#define if_bridge_par_ne(x,y) if_bridge_par_compare(x,y)!=0


typedef struct {
  if_clock_type x1;
  if_clock_type y1;
  if_clock_type z1;
  if_clock_type x2;
  if_clock_type y2;
  if_clock_type z2;
  if_clock_type x3;
  if_clock_type y3;
  if_clock_type z3;
  if_clock_type x4;
  if_clock_type y4;
  if_clock_type z4;
  if_clock_type F;
  if_clock_type Q;
  if_sigs_type sig1;
  if_sigs_type sig2;
  if_sigs_type sig3;
  if_sigs_type sig4;
} if_bridge_var_type;

inline void if_bridge_var_copy
    (if_bridge_var_type& x, const if_bridge_var_type y);
inline int if_bridge_var_compare
    (const if_bridge_var_type x, const if_bridge_var_type y);
inline void if_bridge_var_print
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_xmlize
    (const if_bridge_var_type x, FILE* f);
inline void if_bridge_var_reset
    (if_bridge_var_type& x);
#define if_bridge_var_iterate(x)\
  if_clock_iterate(x.x1)\
  if_clock_iterate(x.y1)\
  if_clock_iterate(x.z1)\
  if_clock_iterate(x.x2)\
  if_clock_iterate(x.y2)\
  if_clock_iterate(x.z2)\
  if_clock_iterate(x.x3)\
  if_clock_iterate(x.y3)\
  if_clock_iterate(x.z3)\
  if_clock_iterate(x.x4)\
  if_clock_iterate(x.y4)\
  if_clock_iterate(x.z4)\
  if_clock_iterate(x.F)\
  if_clock_iterate(x.Q)\
  if_sigs_iterate(x.sig1)\
  if_sigs_iterate(x.sig2)\
  if_sigs_iterate(x.sig3)\
  if_sigs_iterate(x.sig4)
inline if_bridge_var_type if_bridge_var_make
    (if_clock_type x1,if_clock_type y1,if_clock_type z1,if_clock_type x2,if_clock_type y2,if_clock_type z2,if_clock_type x3,if_clock_type y3,if_clock_type z3,if_clock_type x4,if_clock_type y4,if_clock_type z4,if_clock_type F,if_clock_type Q,if_sigs_type sig1,if_sigs_type sig2,if_sigs_type sig3,if_sigs_type sig4);
#define if_bridge_var_eq(x,y) if_bridge_var_compare(x,y)==0
#define if_bridge_var_ne(x,y) if_bridge_var_compare(x,y)!=0

class if_bridge_instance : public if_twosignal_instance {

public:
  
  if_bridge_instance();
  if_bridge_instance(const if_bridge_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& x1()
    { return m_var.x1; }
  inline if_clock_type& y1()
    { return m_var.y1; }
  inline if_clock_type& z1()
    { return m_var.z1; }
  inline if_clock_type& x2()
    { return m_var.x2; }
  inline if_clock_type& y2()
    { return m_var.y2; }
  inline if_clock_type& z2()
    { return m_var.z2; }
  inline if_clock_type& x3()
    { return m_var.x3; }
  inline if_clock_type& y3()
    { return m_var.y3; }
  inline if_clock_type& z3()
    { return m_var.z3; }
  inline if_clock_type& x4()
    { return m_var.x4; }
  inline if_clock_type& y4()
    { return m_var.y4; }
  inline if_clock_type& z4()
    { return m_var.z4; }
  inline if_clock_type& F()
    { return m_var.F; }
  inline if_clock_type& Q()
    { return m_var.Q; }
  inline if_sigs_type& sig1()
    { return m_var.sig1; }
  inline if_sigs_type& sig2()
    { return m_var.sig2; }
  inline if_sigs_type& sig3()
    { return m_var.sig3; }
  inline if_sigs_type& sig4()
    { return m_var.sig4; }
  

public:
  if_bridge_par_type m_par;  /* parameters */ 
  if_bridge_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */
  int m_vertex;
  int listype;


private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();
  inline void _init_1d_fire();
  inline void _init_1e_fire();
  inline void _init_1f_fire();
  inline void _init_1g_fire();
  inline void _init_1h_fire();
  inline void _init_1i_fire();
  inline void _init_1j_fire();
  inline void _init_1k_fire();
  inline void _init_1l_fire();
  inline void _init_1m_fire();
  inline void _init_1n_fire();
  inline void _init_1o_fire();
  inline void _init_1p_fire();
  inline void _init_1q_fire();
  inline void _init_1r_fire();


         void _mainstate_dispatch(IfMessage*);

         void _mainstate_1_fire(IfMessage*);
  inline void _mainstate_1a_fire();
  inline void _mainstate_1b_fire();
  inline void _mainstate_1c_fire();

         void _mainstate_2_fire(IfMessage*);
  inline void _mainstate_2a_fire();
  inline void _mainstate_2b_fire();
  inline void _mainstate_2c_fire();

         void _mainstate_3_fire(IfMessage*);
  inline void _mainstate_3a_fire();
  inline void _mainstate_3b_fire();
  inline void _mainstate_3c_fire();

         void _mainstate_4_fire(IfMessage*);
  inline void _mainstate_4a_fire();
  inline void _mainstate_4b_fire();
  inline void _mainstate_4c_fire();

         void _mainstate_5_fire(IfMessage*);
  inline void _mainstate_5a_fire();
  inline void _mainstate_5b_fire();
  inline void _mainstate_5c_fire();

         void _mainstate_6_fire(IfMessage*);
  inline void _mainstate_6a_fire();
  inline void _mainstate_6b_fire();
  inline void _mainstate_6c_fire();

         void _mainstate_7_fire(IfMessage*);
  inline void _mainstate_7a_fire();
  inline void _mainstate_7b_fire();
  inline void _mainstate_7c_fire();

         void _mainstate_8_fire(IfMessage*);
  inline void _mainstate_8a_fire();
  inline void _mainstate_8b_fire();
  inline void _mainstate_8c_fire();

         void _mainstate_9_fire(IfMessage*);
  inline void _mainstate_9a_fire();
  inline void _mainstate_9b_fire();
  inline void _mainstate_9c_fire();

         void _mainstate_10_fire(IfMessage*);
  inline void _mainstate_10a_fire();
  inline void _mainstate_10b_fire();
  inline void _mainstate_10c_fire();

         void _mainstate_11_fire(IfMessage*);
  inline void _mainstate_11a_fire();
  inline void _mainstate_11b_fire();
  inline void _mainstate_11c_fire();

         void _mainstate_12_fire(IfMessage*);
  inline void _mainstate_12a_fire();
  inline void _mainstate_12b_fire();
  inline void _mainstate_12c_fire();

         void _mainstate_13_fire(IfMessage*);
  inline void _mainstate_13a_fire();
  inline void _mainstate_13b_fire();
  inline void _mainstate_13c_fire();

         void _mainstate_14_fire(IfMessage*);
  inline void _mainstate_14a_fire();
  inline void _mainstate_14b_fire();
  inline void _mainstate_14c_fire();

         void _mainstate_15_fire(IfMessage*);
  inline void _mainstate_15a_fire();
  inline void _mainstate_15b_fire();
  inline void _mainstate_15c_fire();
  inline void _mainstate_15d_fire();
  inline void _mainstate_15e_fire();
  inline void _mainstate_15f_fire();
  inline void _mainstate_15g_fire();
  inline void _mainstate_15h_fire();
  inline void _mainstate_15i_fire();
  inline void _mainstate_15j_fire();
  inline void _mainstate_15k_fire();
  inline void _mainstate_15l_fire();
  inline void _mainstate_15m_fire();
  inline void _mainstate_15n_fire();
  inline void _mainstate_15o_fire();
  inline void _mainstate_15p_fire();
  inline void _mainstate_15q_fire();
  inline void _mainstate_15r_fire();
  inline void _mainstate_15s_fire();

         void _mainstate_16_fire(IfMessage*);
  inline void _mainstate_16a_fire();
  inline void _mainstate_16b_fire();
  inline void _mainstate_16c_fire();
  inline void _mainstate_16d_fire();
  inline void _mainstate_16e_fire();
  inline void _mainstate_16f_fire();
  inline void _mainstate_16g_fire();
  inline void _mainstate_16h_fire();
  inline void _mainstate_16i_fire();
  inline void _mainstate_16j_fire();
  inline void _mainstate_16k_fire();
  inline void _mainstate_16l_fire();
  inline void _mainstate_16m_fire();
  inline void _mainstate_16n_fire();
  inline void _mainstate_16o_fire();
  inline void _mainstate_16p_fire();
  inline void _mainstate_16q_fire();
  inline void _mainstate_16r_fire();
  inline void _mainstate_16s_fire();

         void _mainstate_17_fire(IfMessage*);
  inline void _mainstate_17a_fire();
  inline void _mainstate_17b_fire();
  inline void _mainstate_17c_fire();
  inline void _mainstate_17d_fire();
  inline void _mainstate_17e_fire();
  inline void _mainstate_17f_fire();
  inline void _mainstate_17g_fire();
  inline void _mainstate_17h_fire();
  inline void _mainstate_17i_fire();
  inline void _mainstate_17j_fire();
  inline void _mainstate_17k_fire();
  inline void _mainstate_17l_fire();
  inline void _mainstate_17m_fire();
  inline void _mainstate_17n_fire();
  inline void _mainstate_17o_fire();
  inline void _mainstate_17p_fire();
  inline void _mainstate_17q_fire();
  inline void _mainstate_17r_fire();
  inline void _mainstate_17s_fire();

         void _mainstate_18_fire(IfMessage*);
  inline void _mainstate_18a_fire();
  inline void _mainstate_18b_fire();
  inline void _mainstate_18c_fire();
  inline void _mainstate_18d_fire();
  inline void _mainstate_18e_fire();
  inline void _mainstate_18f_fire();
  inline void _mainstate_18g_fire();
  inline void _mainstate_18h_fire();
  inline void _mainstate_18i_fire();
  inline void _mainstate_18j_fire();
  inline void _mainstate_18k_fire();
  inline void _mainstate_18l_fire();
  inline void _mainstate_18m_fire();
  inline void _mainstate_18n_fire();
  inline void _mainstate_18o_fire();
  inline void _mainstate_18p_fire();
  inline void _mainstate_18q_fire();
  inline void _mainstate_18r_fire();
  inline void _mainstate_18s_fire();

         void _mainstate_19_fire(IfMessage*);
  inline void _mainstate_19a_fire();
  inline void _mainstate_19b_fire();
  inline void _mainstate_19c_fire();
  inline void _mainstate_19d_fire();
  inline void _mainstate_19e_fire();
  inline void _mainstate_19f_fire();
  inline void _mainstate_19g_fire();
  inline void _mainstate_19h_fire();
  inline void _mainstate_19i_fire();
  inline void _mainstate_19j_fire();
  inline void _mainstate_19k_fire();
  inline void _mainstate_19l_fire();
  inline void _mainstate_19m_fire();
  inline void _mainstate_19n_fire();
  inline void _mainstate_19o_fire();
  inline void _mainstate_19p_fire();
  inline void _mainstate_19q_fire();
  inline void _mainstate_19r_fire();
  inline void _mainstate_19s_fire();

         void _mainstate_20_fire(IfMessage*);
  inline void _mainstate_20a_fire();
  inline void _mainstate_20b_fire();
  inline void _mainstate_20c_fire();
  inline void _mainstate_20d_fire();
  inline void _mainstate_20e_fire();
  inline void _mainstate_20f_fire();
  inline void _mainstate_20g_fire();
  inline void _mainstate_20h_fire();
  inline void _mainstate_20i_fire();
  inline void _mainstate_20j_fire();
  inline void _mainstate_20k_fire();
  inline void _mainstate_20l_fire();
  inline void _mainstate_20m_fire();
  inline void _mainstate_20n_fire();
  inline void _mainstate_20o_fire();
  inline void _mainstate_20p_fire();
  inline void _mainstate_20q_fire();
  inline void _mainstate_20r_fire();
  inline void _mainstate_20s_fire();

         void _mainstate_21_fire(IfMessage*);
  inline void _mainstate_21a_fire();
  inline void _mainstate_21b_fire();
  inline void _mainstate_21c_fire();
  inline void _mainstate_21d_fire();
  inline void _mainstate_21e_fire();
  inline void _mainstate_21f_fire();
  inline void _mainstate_21g_fire();
  inline void _mainstate_21h_fire();
  inline void _mainstate_21i_fire();
  inline void _mainstate_21j_fire();
  inline void _mainstate_21k_fire();
  inline void _mainstate_21l_fire();
  inline void _mainstate_21m_fire();
  inline void _mainstate_21n_fire();
  inline void _mainstate_21o_fire();
  inline void _mainstate_21p_fire();
  inline void _mainstate_21q_fire();
  inline void _mainstate_21r_fire();
  inline void _mainstate_21s_fire();

         void _mainstate_22_fire(IfMessage*);
  inline void _mainstate_22a_fire();
  inline void _mainstate_22b_fire();
  inline void _mainstate_22c_fire();
  inline void _mainstate_22d_fire();
  inline void _mainstate_22e_fire();
  inline void _mainstate_22f_fire();
  inline void _mainstate_22g_fire();
  inline void _mainstate_22h_fire();
  inline void _mainstate_22i_fire();
  inline void _mainstate_22j_fire();
  inline void _mainstate_22k_fire();
  inline void _mainstate_22l_fire();
  inline void _mainstate_22m_fire();
  inline void _mainstate_22n_fire();
  inline void _mainstate_22o_fire();
  inline void _mainstate_22p_fire();
  inline void _mainstate_22q_fire();
  inline void _mainstate_22r_fire();
  inline void _mainstate_22s_fire();

         void _mainstate_23_fire(IfMessage*);
  inline void _mainstate_23a_fire();
  inline void _mainstate_23b_fire();

         void _mainstate_24_fire(IfMessage*);
  inline void _mainstate_24a_fire();
  inline void _mainstate_24b_fire();

         void _mainstate_25_fire(IfMessage*);
  inline void _mainstate_25a_fire();
  inline void _mainstate_25b_fire();

         void _mainstate_26_fire(IfMessage*);
  inline void _mainstate_26a_fire();
  inline void _mainstate_26b_fire();

         void _mainstate_27_fire(IfMessage*);
  inline void _mainstate_27a_fire();
  inline void _mainstate_27b_fire();

         void _mainstate_28_fire(IfMessage*);
  inline void _mainstate_28a_fire();
  inline void _mainstate_28b_fire();

         void _mainstate_29_fire(IfMessage*);
  inline void _mainstate_29a_fire();
  inline void _mainstate_29b_fire();

         void _mainstate_30_fire(IfMessage*);
  inline void _mainstate_30a_fire();
  inline void _mainstate_30b_fire();

         void _mainstate_31_fire(IfMessage*);
  inline void _mainstate_31a_fire();
  inline void _mainstate_31b_fire();

         void _mainstate_32_fire(IfMessage*);
  inline void _mainstate_32a_fire();
  inline void _mainstate_32b_fire();

         void _mainstate_33_fire(IfMessage*);
  inline void _mainstate_33a_fire();
  inline void _mainstate_33b_fire();

         void _mainstate_34_fire(IfMessage*);
  inline void _mainstate_34a_fire();
  inline void _mainstate_34b_fire();

         void _mainstate_35_fire(IfMessage*);
  inline void _mainstate_35a_fire();
  inline void _mainstate_35b_fire();

         void _mainstate_36_fire(IfMessage*);
  inline void _mainstate_36a_fire();
  inline void _mainstate_36b_fire();

         void _mainstate_37_fire(IfMessage*);
  inline void _mainstate_37a_fire();
  inline void _mainstate_37b_fire();

         void _mainstate_38_fire(IfMessage*);
  inline void _mainstate_38a_fire();
  inline void _mainstate_38b_fire();

         void _mainstate_39_fire(IfMessage*);
  inline void _mainstate_39a_fire();
  inline void _mainstate_39b_fire();

         void _mainstate_40_fire(IfMessage*);
  inline void _mainstate_40a_fire();
  inline void _mainstate_40b_fire();

         void _mainstate_41_fire(IfMessage*);
  inline void _mainstate_41a_fire();
  inline void _mainstate_41b_fire();

         void _mainstate_42_fire(IfMessage*);
  inline void _mainstate_42a_fire();
  inline void _mainstate_42b_fire();

         void _mainstate_43_fire(IfMessage*);
  inline void _mainstate_43a_fire();
  inline void _mainstate_43b_fire();

         void _mainstate_44_fire(IfMessage*);
  inline void _mainstate_44a_fire();
  inline void _mainstate_44b_fire();

         void _mainstate_45_fire(IfMessage*);
  inline void _mainstate_45a_fire();
  inline void _mainstate_45b_fire();

         void _mainstate_46_fire(IfMessage*);
  inline void _mainstate_46a_fire();
  inline void _mainstate_46b_fire();

         void _mainstate_47_fire(IfMessage*);
  inline void _mainstate_47a_fire();
  inline void _mainstate_47b_fire();
  inline void _mainstate_47c_fire();
  inline void _mainstate_47d_fire();

         void _mainstate_48_fire(IfMessage*);
  inline void _mainstate_48a_fire();
  inline void _mainstate_48b_fire();
  inline void _mainstate_48c_fire();
  inline void _mainstate_48d_fire();

         void _mainstate_49_fire(IfMessage*);
  inline void _mainstate_49a_fire();
  inline void _mainstate_49b_fire();
  inline void _mainstate_49c_fire();
  inline void _mainstate_49d_fire();

         void _mainstate_50_fire(IfMessage*);
  inline void _mainstate_50a_fire();
  inline void _mainstate_50b_fire();
  inline void _mainstate_50c_fire();
  inline void _mainstate_50d_fire();

         void _mainstate_51_fire(IfMessage*);
  inline void _mainstate_51a_fire();
  inline void _mainstate_51b_fire();
  inline void _mainstate_51c_fire();
  inline void _mainstate_51d_fire();

         void _mainstate_52_fire(IfMessage*);
  inline void _mainstate_52a_fire();
  inline void _mainstate_52b_fire();
  inline void _mainstate_52c_fire();
  inline void _mainstate_52d_fire();

         void _mainstate_53_fire(IfMessage*);
  inline void _mainstate_53a_fire();
  inline void _mainstate_53b_fire();
  inline void _mainstate_53c_fire();
  inline void _mainstate_53d_fire();

         void _mainstate_54_fire(IfMessage*);
  inline void _mainstate_54a_fire();
  inline void _mainstate_54b_fire();
  inline void _mainstate_54c_fire();
  inline void _mainstate_54d_fire();

         void _mainstate_55_fire(IfMessage*);
  inline void _mainstate_55a_fire();
  inline void _mainstate_55b_fire();
  inline void _mainstate_55c_fire();
  inline void _mainstate_55d_fire();

         void _mainstate_56_fire(IfMessage*);
  inline void _mainstate_56a_fire();
  inline void _mainstate_56b_fire();
  inline void _mainstate_56c_fire();
  inline void _mainstate_56d_fire();

         void _mainstate_57_fire(IfMessage*);
  inline void _mainstate_57a_fire();
  inline void _mainstate_57b_fire();
  inline void _mainstate_57c_fire();
  inline void _mainstate_57d_fire();

         void _mainstate_58_fire(IfMessage*);
  inline void _mainstate_58a_fire();
  inline void _mainstate_58b_fire();
  inline void _mainstate_58c_fire();
  inline void _mainstate_58d_fire();

         void _mainstate_59_fire(IfMessage*);
  inline void _mainstate_59a_fire();
  inline void _mainstate_59b_fire();
  inline void _mainstate_59c_fire();
  inline void _mainstate_59d_fire();

         void _mainstate_60_fire(IfMessage*);
  inline void _mainstate_60a_fire();
  inline void _mainstate_60b_fire();
  inline void _mainstate_60c_fire();
  inline void _mainstate_60d_fire();

         void _mainstate_61_fire(IfMessage*);
  inline void _mainstate_61a_fire();
  inline void _mainstate_61b_fire();
  inline void _mainstate_61c_fire();
  inline void _mainstate_61d_fire();

         void _mainstate_62_fire(IfMessage*);
  inline void _mainstate_62a_fire();
  inline void _mainstate_62b_fire();
  inline void _mainstate_62c_fire();
  inline void _mainstate_62d_fire();

         void _mainstate_63_fire(IfMessage*);
  inline void _mainstate_63a_fire();
  inline void _mainstate_63b_fire();
  inline void _mainstate_63c_fire();
  inline void _mainstate_63d_fire();

         void _mainstate_64_fire(IfMessage*);
  inline void _mainstate_64a_fire();
  inline void _mainstate_64b_fire();
  inline void _mainstate_64c_fire();
  inline void _mainstate_64d_fire();

         void _mainstate_65_fire(IfMessage*);
  inline void _mainstate_65a_fire();
  inline void _mainstate_65b_fire();
  inline void _mainstate_65c_fire();
  inline void _mainstate_65d_fire();

         void _mainstate_66_fire(IfMessage*);
  inline void _mainstate_66a_fire();
  inline void _mainstate_66b_fire();
  inline void _mainstate_66c_fire();
  inline void _mainstate_66d_fire();

         void _mainstate_67_fire(IfMessage*);
  inline void _mainstate_67a_fire();
  inline void _mainstate_67b_fire();
  inline void _mainstate_67c_fire();
  inline void _mainstate_67d_fire();

         void _mainstate_68_fire(IfMessage*);
  inline void _mainstate_68a_fire();
  inline void _mainstate_68b_fire();
  inline void _mainstate_68c_fire();
  inline void _mainstate_68d_fire();

         void _mainstate_69_fire(IfMessage*);
  inline void _mainstate_69a_fire();
  inline void _mainstate_69b_fire();
  inline void _mainstate_69c_fire();
  inline void _mainstate_69d_fire();

         void _mainstate_70_fire(IfMessage*);
  inline void _mainstate_70a_fire();
  inline void _mainstate_70b_fire();
  inline void _mainstate_70c_fire();
  inline void _mainstate_70d_fire();

         void _mainstate_71_fire(IfMessage*);
  inline void _mainstate_71a_fire();
  inline void _mainstate_71b_fire();
  inline void _mainstate_71c_fire();
  inline void _mainstate_71d_fire();

         void _mainstate_72_fire(IfMessage*);
  inline void _mainstate_72a_fire();
  inline void _mainstate_72b_fire();
  inline void _mainstate_72c_fire();
  inline void _mainstate_72d_fire();

         void _mainstate_73_fire(IfMessage*);
  inline void _mainstate_73a_fire();
  inline void _mainstate_73b_fire();
  inline void _mainstate_73c_fire();
  inline void _mainstate_73d_fire();

         void _mainstate_74_fire(IfMessage*);
  inline void _mainstate_74a_fire();
  inline void _mainstate_74b_fire();
  inline void _mainstate_74c_fire();
  inline void _mainstate_74d_fire();

         void _mainstate_75_fire(IfMessage*);
  inline void _mainstate_75a_fire();
  inline void _mainstate_75b_fire();
  inline void _mainstate_75c_fire();
  inline void _mainstate_75d_fire();

         void _mainstate_76_fire(IfMessage*);
  inline void _mainstate_76a_fire();
  inline void _mainstate_76b_fire();
  inline void _mainstate_76c_fire();
  inline void _mainstate_76d_fire();

         void _mainstate_77_fire(IfMessage*);
  inline void _mainstate_77a_fire();
  inline void _mainstate_77b_fire();
  inline void _mainstate_77c_fire();
  inline void _mainstate_77d_fire();

         void _mainstate_78_fire(IfMessage*);
  inline void _mainstate_78a_fire();
  inline void _mainstate_78b_fire();
  inline void _mainstate_78c_fire();
  inline void _mainstate_78d_fire();

         void _mainstate_79_fire(IfMessage*);
  inline void _mainstate_79a_fire();
  inline void _mainstate_79b_fire();
  inline void _mainstate_79c_fire();
  inline void _mainstate_79d_fire();

         void _mainstate_80_fire(IfMessage*);
  inline void _mainstate_80a_fire();
  inline void _mainstate_80b_fire();
  inline void _mainstate_80c_fire();
  inline void _mainstate_80d_fire();

         void _mainstate_81_fire(IfMessage*);
  inline void _mainstate_81a_fire();
  inline void _mainstate_81b_fire();
  inline void _mainstate_81c_fire();
  inline void _mainstate_81d_fire();

         void _mainstate_82_fire(IfMessage*);
  inline void _mainstate_82a_fire();
  inline void _mainstate_82b_fire();
  inline void _mainstate_82c_fire();
  inline void _mainstate_82d_fire();


         void _deadlock_dispatch(IfMessage*);

private:
  typedef void (if_bridge_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_twosignal_iterator : public IfIterator {

  public:
    if_twosignal_iterator();
  
  public:
    virtual IfConfig* start();

};


