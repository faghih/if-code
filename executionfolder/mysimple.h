


/*
 *
 * IF-2.0 Intermediate Representation.
 * 
 * Copyright (C) Verimag Grenoble.
 *
 * Marius Bozga
 *
 */









/* 
 * fathiyeh [system] instance interface 
 *
 */

class if_fathiyeh_instance : public IfInstance { 

  public:
    if_fathiyeh_instance(if_pid_type = 0, IfQueue* = IfQueue::NIL);
    if_fathiyeh_instance(const if_fathiyeh_instance&);

  protected:

};


/* 
 * fathproce instance interface 
 *
 */
 
#define if_fathproce_process 1



typedef if_void_type if_fathproce_par_type;

#define if_fathproce_par_copy(x,y) if_void_copy(x,y)
#define if_fathproce_par_compare(x,y) if_void_compare(x,y)
#define if_fathproce_par_print(x,f) if_void_print(x,f)
#define if_fathproce_par_xmlize(x,f) if_void_xmlize(x,f)
#define if_fathproce_par_reset(x) if_void_reset(x)
#define if_fathproce_par_iterate(x) if_void_iterate(x)
#define if_fathproce_par_eq(x,y) if_fathproce_par_compare(x,y)==0
#define if_fathproce_par_ne(x,y) if_fathproce_par_compare(x,y)!=0


typedef struct {
  if_clock_type x1;
} if_fathproce_var_type;

inline void if_fathproce_var_copy
    (if_fathproce_var_type& x, const if_fathproce_var_type y);
inline int if_fathproce_var_compare
    (const if_fathproce_var_type x, const if_fathproce_var_type y);
inline void if_fathproce_var_print
    (const if_fathproce_var_type x, FILE* f);
inline void if_fathproce_var_xmlize
    (const if_fathproce_var_type x, FILE* f);
inline void if_fathproce_var_reset
    (if_fathproce_var_type& x);
#define if_fathproce_var_iterate(x)\
  if_clock_iterate(x.x1)
inline if_fathproce_var_type if_fathproce_var_make
    (if_clock_type x1);
#define if_fathproce_var_eq(x,y) if_fathproce_var_compare(x,y)==0
#define if_fathproce_var_ne(x,y) if_fathproce_var_compare(x,y)!=0

class if_fathproce_instance : public if_fathiyeh_instance {

public:
  
  if_fathproce_instance();
  if_fathproce_instance(const if_fathproce_instance&);

public:
  virtual const char* getState() const;
  virtual int has(const unsigned) const;
  virtual int is(const unsigned) const;

public:
  virtual int compare(const IfInstance*) const;
  virtual unsigned hash(const unsigned) const;
  virtual IfInstance* copy() const;
  virtual void print(FILE*) const;
  virtual void xmlize(FILE*) const;

public:	
  virtual void reset();
  virtual void iterate(IfIterator*);
  virtual void copy(const IfInstance*);

public:
  static const char* NAME;

public:
  inline if_clock_type& x1()
    { return m_var.x1; }
  

private: 
  if_fathproce_par_type m_par;  /* parameters */ 
  if_fathproce_var_type m_var;  /* variables */
  int m_sp;  /* state pointer */

private:

         void _main_dispatch(IfMessage*);

         void _init_dispatch(IfMessage*);

         void _init_1_fire(IfMessage*);
  inline void _init_1a_fire();
  inline void _init_1b_fire();
  inline void _init_1c_fire();


         void _firststate_dispatch(IfMessage*);

         void _firststate_1_fire(IfMessage*);
  inline void _firststate_1a_fire();
  inline void _firststate_1b_fire();


         void _secondstate_dispatch(IfMessage*);

         void _secondstate_1_fire(IfMessage*);
  inline void _secondstate_1a_fire();
  inline void _secondstate_1b_fire();
  inline void _secondstate_1c_fire();


private:
  typedef void (if_fathproce_instance::*dispatcher)(IfMessage*);
  static if_state<1,dispatcher> STATE[];

  inline int input(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 1; }
  inline int save(unsigned signal) 
    { return STATE[m_sp].sigtab[signal] & 2; }

private:
         void fire();
  inline void nextstate(int);

};


/*
 * [system] iterator interface
 *
 */ 

class if_fathiyeh_iterator : public IfIterator {

  public:
    if_fathiyeh_iterator();
  
  public:
    virtual IfConfig* start();

};


