#include <iostream>
#include <string>

template<typename Fun>
class FindSubSets
{
    std::string set;
    Fun callback;

    std::string prefix;

    void find_recursive(int pos)
    {
        if (pos == set.length())
        {
            callback(prefix);
        }
        else
        {
            prefix.push_back(set[pos]);
            find_recursive(pos + 1);
            prefix.resize(prefix.length() - 1);

            find_recursive(pos + 1);
        }
    }

public:
    FindSubSets(const std::string& set, Fun callback)
    : set(set), callback(callback), prefix()
    {
        find_recursive(0);
    }
};

typedef void (*funptr)(const std::string&);

void output(const std::string& s)
{
    std::cout << s << std::endl;
}

struct Hollywood
{
    void operator()(const std::string& s)
    {
        std::cout << s << std::endl;
    }
};

/*int main()
{
    std::cout << "via function pointer:" << std::endl;
    FindSubSets<funptr>("ABCDE", output);

    std::cout << "via functor:" << std::endl;
    FindSubSets<Hollywood>("ABCDE", Hollywood());
}*/
