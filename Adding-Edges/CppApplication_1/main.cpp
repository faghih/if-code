/* 
 * File:   main.cpp
 * Author: ffaghihe
 *
 * Created on March 4, 2013, 1:46 PM
 */

#include <stdlib.h>
#include <list>
#include <hash_set>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
 #include <sstream>
//#include "subsetcal.h"

using namespace std;

/*
 * 
 */

 typedef vector<string> subsettyp;
 vector<subsettyp> allsubsets1;

template<typename Fun>
class FindSubSets
{
    vector<string> set;
    Fun callback;

    subsettyp prefix;
 //   static int counter;

    void find_recursive(int pos)
    {
        if (pos == set.size())
        {
            callback(prefix);
        }
        else
        {
            prefix.push_back(set[pos]);
            find_recursive(pos + 1);
            prefix.resize(prefix.size() - 1);

            find_recursive(pos + 1 );
        }
    }

public:
    FindSubSets(const vector<string>& set, Fun callback)
    : set(set), callback(callback), prefix()
    {
        find_recursive(0);
    }
};

typedef void (*funptr)(const subsettyp& subset);

void output(const std::string& s)
{
    std::cout <<  s << std::endl;
}
void myfunctor( const subsettyp& subset)
{
    allsubsets1.push_back(subset);
}

struct Hollywood
{
    void operator()(const std::string& s)
    {
        std::cout << s << std::endl;
    }
};


struct clause{
        string location;
        string vald;
        string valc;
    };

enum signal {
      R,
      G,
      Y
   };
  // list <vector <string>>

  

/*vector <subsettyp> find_allsubsets (vector<string> inputset){
     //int siz = pow (2,inputset.size());
     vector <subsettyp> allsubsets;
     if(inputset.size()==1){
       //  subsettyp newsubset;
      //   newsubset.push_back(inputset.front());
         allsubsets.push_back(inputset);
         return allsubsets;
     }
     else{
      //  subsettyp newsubset;
        allsubsets.push_back(inputset);
        for(int i=0;i<inputset.size();i++){
            vector<string> thisset = inputset;
            thisset.erase(thisset.begin() + i);
            vector <subsettyp> allsubsetsi = find_allsubsets(thisset);
            for(int j=0;j<allsubsetsi.size();j++){
                allsubsets.push_back(allsubsetsi.at(j));
            }
        }
        return allsubsets;
        //allsubsets.push_back(newsubset);
     }
}*/

   typedef std::vector<char> Vi;
typedef std::vector<Vi> Vvi;

// Just for the sample -- populate the intput data set
Vvi build_input(int siz) {
   Vvi vvi;

   for(int i = 0; i < siz; i++) {
      Vi vi;
      
      vi.push_back('G');
      vi.push_back('R');
      vi.push_back('Y');
     
      vvi.push_back(vi);
   }
   return vvi;
}

// just for the sample -- print the data sets
std::ostream&
operator<<(std::ostream& os, const Vi& vi)
{
  os << "(";
  std::copy(vi.begin(), vi.end(), std::ostream_iterator<char>(os, ", "));
  os << ")";
  return os;
}
std::ostream&
operator<<(std::ostream& os, const Vvi& vvi)
{
  os << "(\n";
  for(Vvi::const_iterator it = vvi.begin();
      it != vvi.end();
      it++) {
      os << "  " << *it << "\n";
  }
  os << ")";
  return os;
}

// recursive algorithm to to produce cart. prod.
// At any given moment, "me" points to some Vi in the middle of the
// input data set.
//   for int i in *me:
//      add i to current result
//      recurse on next "me"
//
void cart_product(
    Vvi& rvvi,  // final result
    Vi&  rvi,   // current result
    Vvi::const_iterator me, // current input
    Vvi::const_iterator end) // final input
{
    if(me == end) {
        // terminal condition of the recursion. We no longer have
        // any input vectors to manipulate. Add the current result (rvi)
        // to the total set of results (rvvvi).
        rvvi.push_back(rvi);
        return;
    }

    // need an easy name for my vector-of-ints
    const Vi& mevi = *me;
    for(Vi::const_iterator it = mevi.begin();
        it != mevi.end();
        it++) {
        // final rvi will look like "a, b, c, ME, d, e, f"
        // At the moment, rvi already has "a, b, c"
        rvi.push_back(*it);  // add ME
        cart_product(rvvi, rvi, me+1, end);
        rvi.pop_back(); // clean ME off for next round
    }
}
string int2string(int number)
{

    std::ostringstream ostr; //output string stream
    ostr << number; //use the string stream just like cout,
    //except the stream prints not to stdout but to a string.

    std::string theNumberString = ostr.str(); //the str() function of the stream
    //returns the string.

    return theNumberString;

    //now  theNumberString is "1234"
}
string char2string(char number)
{

    std::ostringstream ostr; //output string stream
    ostr << number; //use the string stream just like cout,
    //except the stream prints not to stdout but to a string.

    std::string theNumberString = ostr.str(); //the str() function of the stream
    //returns the string.

    return theNumberString;

    //now  theNumberString is "1234"
}

int find_loc(string loca,string mystring){
    int i = 0;
    while(true){
      int location = mystring.find( loca , i );
      if(mystring.substr(location-10,9) != "nextstate"){
          return location+loca.size()+1;
      }
      else{
          i++;
      }
    }

}

int main(int argc, char** argv) {
   //input: a file, LS (a set of conjunctions 
   //                     (each is a three element tuple: location, varibale, disjunction of clock constraints)),
   //         Q (a set of conjunctions),
    //        clocks, variables

     //Output: in eahc location, add all the switches for !LS, and Q, as long as the target is in Q or LS.

    
    const char* filename = "twosignalfaulty_1_woreset.if";
    const char* newfilename = "twosig_edged.if";
    


    vector <string> locations;
    vector <string> clocks;
    int signum = 2;
    locations.push_back("mainstate");
  //  locations.push_back("execution");
    clocks.push_back("x1");
    clocks.push_back("y1");
    clocks.push_back("z1");
    clocks.push_back("x2");
    clocks.push_back("y2");
    clocks.push_back("z2");
    vector<clause> clauselist;
    clause cl1;
    cl1.location = "mainstate";
    cl1.vald = "sig1 = G and sig2 = R";
    cl1.valc = "z1<2";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = G and sig2 = R";
    cl1.valc = "x1>10";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = G and not(sig2 = R)";
    cl1.valc = "";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = Y and not(sig2 = R)";
    cl1.valc = "";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = Y and sig2 = R";
    cl1.valc = "z1<2";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = G and sig2 = R";
    cl1.valc = "y1>2";
    clauselist.push_back(cl1);
    cl1.vald = "sig1 = R and sig2 = R";
    cl1.valc = "z1<=1 and z2<=1";
     cl1.vald = "sig2 = G and sig1 = R";
    cl1.valc = "z2<2";
    clauselist.push_back(cl1);
    cl1.vald = "sig2 = G and sig1 = R";
    cl1.valc = "x2>10";
    clauselist.push_back(cl1);
    cl1.vald = "sig2 = G and not(sig1 = R)";
    cl1.valc = "";
    clauselist.push_back(cl1);
    cl1.vald = "sig2 = Y and not(sig1 = R)";
    cl1.valc = "";
    clauselist.push_back(cl1);
    cl1.vald = "sig2 = Y and sig1 = R";
    cl1.valc = "z2<2";
    clauselist.push_back(cl1);
    cl1.vald = "sig2 = G and sig1 = R";
    cl1.valc = "y2>2";
    clauselist.push_back(cl1);
   // char* location = "execution";
    
   


    Vvi input(build_input(signum));
    std::cout << input << "\n";

    Vvi varscomb;
    Vi outputTemp;
    cart_product(varscomb, outputTemp, input.begin(), input.end());

    std::cout << output << "\n";
    ifstream myfile;
    myfile.open (filename);
    /*myfile.seekg(0,std::ios::end);
    std::streampos  length = myfile.tellg();
    myfile.seekg(0,std::ios::beg);
    std::vector<char> buffer(length);
    myfile.read(&buffer[0],length);
    char* buf = std::copy(buffer.begin(), buffer.end(), buf);*/
    string mystring,tmp;
    
    while(!myfile.eof()) {
    getline(myfile, tmp);   
    mystring += tmp;
    mystring += "\n";
    }
    
    myfile.close();
    //string mystring (buffer);
    //string tobeadded= "";
   vector<string> allsubsets;
   FindSubSets<funptr>(clocks, myfunctor);
   std::cout << allsubsets1.size() <<std::endl;
   for(int z=0;z<allsubsets1.size();z++){
            vector<string> cls = allsubsets1.at(z);
            string resetcls ="";
            for (int h=0;h<cls.size();h++){
                std::cout << cls.at(h) << ",";

            }
            std::cout << std::endl;
        }
   string tran;
 //  vector <subsettyp> clockscomb = find_allsubsets (clocks);
    
    int loc = find_loc("mainstate",mystring);
    for(int i =0; i<clauselist.size() ; i++){
        string newst="";
        clause thisclause = clauselist.at(i);
        string loca = thisclause.location;
      //  int loc = find_loc(loca,mystring); ///note: use this for automaton with more than one state
       // string::size_type loc = mystring.find( loca , 0 )+loca.size()+1;
        newst+="\n deadline lazy;";
        newst+="\n provided ";
        newst+=thisclause.vald;
        newst+=";\n";
      /*  if(thisclause.valc != ""){
          newst+="when ";
          newst+=thisclause.valc;
          newst+=";\n";
        }*/
       ////  for(int x=0;x<locations.size();x++){
          for(int z=0;z<allsubsets1.size();z++){
       ////      for(int l=0;l<varscomb.size();l++){

     ////       Vi thisvarscomb = varscomb.at(l);
     ////        string varassign ="";
     ////       for (int h=0;h<signum;h++){
    ////            varassign += "task sig";
    ////            varassign += int2string(h+1);
    ////            varassign += " := ";
   ////             varassign += char2string(thisvarscomb.at(h));
  ////              varassign += ";\n";
   ////         }
            vector<string> cls = allsubsets1.at(z);
            string resetcls ="";
  ////           resetcls += varassign;
            for (int h=0;h<cls.size();h++){
                resetcls += "set ";
                resetcls += cls.at(h);
                resetcls += " := 0;\n";
            }
            tran+=newst;
          //  for (int j=0;j<signum;j++){
                tran+=resetcls;
                tran+="nextstate ";
                tran+=loca;
                tran+="; \n";
                mystring.insert(loc,tran);
                std::cout << "tran:::::" << tran << std::endl;
                tran="";
                
           }
          }
       // }
      //  clauselist.pop_front();
   // }
    
   fstream filestr;

   filestr.open (newfilename, fstream::in | fstream::out);

   filestr<<mystring<<endl;
   filestr.close();
   return (EXIT_SUCCESS);
}

