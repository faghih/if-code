/*
 * File:   myBfsExplorer.h
 * Author: ffaghihe
 *
 * Created on February 7, 2013, 4:36 PM
 */

#ifndef _MYBFSEXPLORER_H
#define	_MYBFSEXPLORER_H

#endif	/* _MYBFSEXPLORER_H */

#include "simulator.h"
#include "threesignal_lessswitches.h"
#include <iostream>
#include <queue>
#include <vector>

  #include <utility>                   // for std::pair
  #include <algorithm>                 // for std::for_each
 // #include <boost/graph/graph_traits.hpp>
  #include <boost/graph/adjacency_list.hpp>
  #include <boost/unordered_map.hpp>
  //#include <boost/graph/dijkstra_shortest_paths.hpp>


using namespace boost;
using namespace std;
clock_t starttime,endtime;

class myBfsExplorer : public IfDriver {
  static const int REACHED = 1; // reachable state marking
  static const int ss = 1;
  static const int qq = 2;
  static const int nots = 3;
  static const int sw = 4;
  static const int qw = 5;
  static const int notsw = 6;
  static const if_pid_type exewait = 0;
  std::queue<IfConfig*>  m_queue; // the queue of unexplored states
  //typedef adjacency_list<listS, vecS, bidirectionalS> Graph;
 // typedef std::pair<IfConfig*, IfConfig*> Edge;
  //Graph zonegraph(1);
  struct vertexinfo{
    int vertexid;
  };
  typedef adjacency_list<listS, vecS, bidirectionalS, vertexinfo> Graph;
  boost::unordered_map<IfConfig*, int> myhashmap;
  //typedef std::pair<IfConfig*, IfConfig*> Edge;
  //typedef adjacency_list<vecS, vecS,bidirectionalS, IfConfig*, Edge> Graph;
 // vector<IfConfig*> V;
  Graph g;
  // typedef typename boost::graph_traits<g>::vertex_descriptor vertex_descriptor;
  int id;
  
public:

  myBfsExplorer(IfEngine* engine) : IfDriver(engine)  {}
  virtual ~myBfsExplorer() { }





   
  // successor handler: append target state to the queue, if not yet reached
  void explore(IfConfig* source, IfLabel* label, IfConfig* target) {
      
     // add_edge(source,target, g);
    //add_edge(source, target, zonegraph);
    if (! (target->getMark() & REACHED) )
      {
        
        target->setMark(REACHED); m_queue.push(target); }
   
  }
  // visit one state i.e, print it on the screen
  void visit(IfConfig* state) {
    std::cout << std::endl << "printing..." << std::endl;
    state->print(stdout);
    IfDbmTime* mydbm = (IfDbmTime*) state->getTime();
  /*   if_type_zone z1 = mydbm->m_dbm;
    if_type_zone z2 = if_backward_zone(z1);

    std::cout << "printing zone:" << std::endl;
    if_print_zone(z1,stdout);
     std::cout << "printing down zone:" << std::endl;
    if_print_zone(z2,stdout);*/
  //      if_type_zone z1 = mydbm->m_dbm;
        std::cout << "printing zone:" << std::endl;
    //s   std::cout << std::endl << "is zone empty?:" <<if_is_empty_zone(z1)<< std::endl;
   // std::cout << std::endl << "printing zone:" << std::endl;
   // if_print_zone(z1,stdout);
   // std::cout << std::endl << "is zone empty?:" <<if_is_empty_zone(z1)<< std::endl;
  //  std::cout << std::endl << "printing time:" << std::endl;
    //mydbm->timeval()
 //   mydbm->print(stdout);
   // if_type_zone* z1 = (if_type_zone*) mydbm;
   // if_print_zone(if_extrapolate_zone(z1),stdout);
  }

  // visit all states, main bfs loop

  void visitall () {
 //   if_pid_type mypid = if_pid_mk(if_exewait_process,0);
    std::cout << std::endl << "before start" << std::endl;
    IfConfig* start = m_engine->start();
   // V.push_back(start);
   
    //start->vertex_num=id;
 //   start->listype = ss;
  //  visit(start);
    //////////////////////////////////////////
    
//   if_exewait_instance* myins = (if_exewait_instance*) start->get(mypid);
        //int pri = myins->getPriority();
       // std::cout << "here" << std::endl;
        // myins->print(stdout);
    
    ///////////////////////////////////////////
    start->setMark(REACHED); m_queue.push(start);
    int numm=0;
    while (! m_queue.empty()) {
    //  std::cout << std::endl << "in while:" << numm << std::endl;
    //  numm++;
      IfConfig* state = m_queue.front();//ff: each state in the semantic model is of type ifConfig
      m_queue.pop();
   //  visit(state);
      m_engine->run(state);//ff: it possibly executes "explore" for all the children
    }
     
  }
};



int main(int argc, char* argv[]) {
    starttime=clock();
  int K = 5; // warning: change this value to enable zone extrapolation with K...

  // parse & use command-line arguments, if any...

  // some extra-initialization for dbm time
  if_init_zone_module(); // used for dbm time only
  if (K > 0) if_restrict_extrapolation_zone(K);
  
  // create the base exploration engine
  IfEngine* engine = IfIterator::Create();

  // for the zone graph to only contain jump transitions
  engine = new IfActionTimeFilter(engine);
  
  // create and connect to the driver
  myBfsExplorer bfs(engine);


  

  // run the driver
  bfs.visitall();
  
  // delete the engine
  delete engine;

  // cleanup
  if_exit_zone_module();

  endtime = clock();
     float diff = (((float)endtime - (float)starttime) / CLOCKS_PER_SEC  );
       std::cout << std::endl << "zone graph generation time: " << std::endl;
     printf("%f",diff);
    // Make convenient labels for the vertices
   
    // declare a graph object
    

    // add the edges to the graph object
 //   for (int i = 0; i < num_edges; ++i)
 //     add_edge(edge_array[i].first, edge_array[i].second, g);

  return 0;
}
